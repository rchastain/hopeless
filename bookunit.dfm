object BookEditor: TBookEditor
  Left = 595
  Top = 1
  ClientHeight = 475
  ClientWidth = 175
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mnBook
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btBad: TSpeedButton
    Left = 50
    Top = 250
    Width = 20
    Height = 21
    Caption = '?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = btBadClick
  end
  object btDubious: TSpeedButton
    Left = 70
    Top = 250
    Width = 20
    Height = 21
    Caption = '?!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = btDubiousClick
  end
  object btNeutral: TSpeedButton
    Left = 90
    Top = 250
    Width = 20
    Height = 21
    OnClick = btNeutralClick
  end
  object btFine: TSpeedButton
    Left = 110
    Top = 250
    Width = 20
    Height = 21
    Caption = '!?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = btFineClick
  end
  object btGreat: TSpeedButton
    Left = 130
    Top = 250
    Width = 20
    Height = 21
    Caption = '!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = btGreatClick
  end
  object btDelete: TSpeedButton
    Left = 150
    Top = 250
    Width = 20
    Height = 21
    Caption = 'X'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = btDeleteClick
  end
  object Label1: TLabel
    Left = 8
    Top = 280
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object sgBook: TStringGrid
    Left = 5
    Top = 5
    Width = 165
    Height = 240
    ColCount = 3
    DefaultColWidth = 40
    DefaultRowHeight = 18
    FixedCols = 0
    RowCount = 12
    FixedRows = 0
    TabOrder = 0
    OnDblClick = sgBookDblClick
    OnMouseDown = sgBookMouseDown
  end
  object edMove: TEdit
    Left = 5
    Top = 250
    Width = 40
    Height = 21
    TabOrder = 1
  end
  object btBlank: TButton
    Left = 90
    Top = 275
    Width = 40
    Height = 22
    Caption = 'BLANK'
    TabOrder = 2
    OnClick = btBlankClick
  end
  object btFinal: TButton
    Left = 130
    Top = 275
    Width = 40
    Height = 22
    Caption = 'FINAL'
    TabOrder = 3
    OnClick = btFinalClick
  end
  object mmComment: TMemo
    Left = 5
    Top = 300
    Width = 165
    Height = 140
    TabOrder = 4
  end
  object btValidate: TButton
    Left = 5
    Top = 445
    Width = 80
    Height = 25
    Caption = 'Validate'
    TabOrder = 5
    OnClick = btValidateClick
  end
  object btSave: TButton
    Left = 90
    Top = 445
    Width = 80
    Height = 25
    Caption = 'Save'
    TabOrder = 6
    OnClick = btSaveClick
  end
  object mnBook: TMainMenu
    Left = 40
    Top = 328
    object miOptions: TMenuItem
      Caption = 'Options'
      object miPopulate: TMenuItem
        AutoCheck = True
        Caption = 'Feed from board'
        OnClick = miPopulateClick
      end
      object miSave: TMenuItem
        Caption = 'Save now'
        OnClick = miSaveClick
      end
      object miSelectedBook: TMenuItem
        Caption = 'Edit'
        object miMain: TMenuItem
          Caption = 'Main book'
          Checked = True
          RadioItem = True
          OnClick = miMainClick
        end
        object miGuide: TMenuItem
          Caption = 'Guide book'
          RadioItem = True
          OnClick = miGuideClick
        end
      end
    end
    object miFind: TMenuItem
      Caption = 'Find'
      object miRandomLeaf: TMenuItem
        Caption = 'Random leaf'
        OnClick = miRandomLeafClick
      end
      object miAlternative: TMenuItem
        Caption = 'Check alternatives'
        OnClick = miAlternativeClick
      end
    end
  end
end
