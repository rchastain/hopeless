
// Maximal gain from a capture, used for delta pruning

function TSearch.MaxCaptValue( SqFr, SqTo : Integer; Immediate : Boolean ) : Integer;
begin
    Result := Cap_Bonus[ Board.List[ Board.Index[ SqTo ] ].Kind ];
    if  ( DO_STATIC )
    and ( not Immediate )
       then begin
         if PawnDefends( SqTo )
            then Result := Result + 200 - Cap_Bonus[ Board.List[ Board.Index[ SqFr ] ].Kind ];
       end;
end; // CaptValue

function TSearch.Quiesc(Ply, Alfa, Beta : Integer; Immediate : Boolean ) : Integer;
         var A, Temp, Best, LocalIndex, GroundVal : Integer;
         LocalMoveList : TMoveSet;
         Current  : TMove;

procedure PickMove;
          var A, Max, Where : Integer;
begin
  Max := Low( Integer );
  Where := 1;

  for A := 1 to LocalIndex do
  if  ( LocalMoveList[ A ].Order > Max )
  and ( LocalMoveList[ A ].Flag < NOFLAG )
  then begin
    Where := A;
    Max := LocalMoveList[ A ].Order;
  end;

  Current := LocalMoveList[ Where ];
  LocalMoveList[ Where ].Flag := NOFLAG;
end; // PickMove

begin  // Quiesc
  Inc( MyStat.Nodes );
  {$IFDEF FULL_STATS} Inc( MyStat.QNodes ); {$ENDIF}
  Best := MyEval.Eval( TRUE );

  if Ply >= MAX_DEPTH - 1
     then begin
        Result := Best;
        Exit;
     end;

  if Best >= Beta
     then Result := Beta
     else begin

       GroundVal := Best;
       GenCapt;

       if KingEnPrise
          then begin
            Result := MATE_VALUE - 100;
            Exit;
          end;

       LocalMoveList := MoveSet;
       LocalIndex := MoveNo;

       for A := 1 to LocalIndex do
       begin // loop
         PickMove;

       // Delta cutoff - program doesn't try captures  /
       // which cannot raise the score. The technique  /
       // requires  certain margin  taking  care  for  /
       // the positional gains.                        /

         if  ( Current.Flag = CAPT )
         and ( GroundVal + MaxCaptValue( Current.SqFr, Current.SqTo, Immediate ) < Alfa - DELTA_MARGIN )
         then begin
           {$IFDEF FULL_STATS} Inc( MyStat.DeltaCuts ); {$ENDIF}
           Continue;
         end;

         MakeMove( Current );
         Temp := -Quiesc(Ply + 1, -Beta, -Alfa, FALSE );
         UnMakeMove;

         if Temp > Best
            then Best := Temp;

         if Best >= Beta
            then Break;

         if Best > Alfa
            then Alfa := Best;
       end; // loop

       Result := Best;
     end;
end; // Quiesc