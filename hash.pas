
function InitHashValue : Integer;
         var A : Integer;
begin
  if Board.Side = WHITE
     then Result := 0
     else Result := 1234567890;

  for A := -16 to -1 do
  if Board.List[ A ].Sqr <> OUTSIDE
     then Result := Result xor HashData[ Board.List[ A ].Sqr, BLACK, Board.List[ A ].Kind];

 for A := 1 to 16 do
 if Board.List[ A ].Sqr <> OUTSIDE
     then Result := Result xor HashData[ Board.List[ A ].Sqr, WHITE, Board.List[ A ].Kind];

 // castling rights
 if Board.Index[ WKMoved ] = 0
    then begin
      if Board.Index[ WKRMoved ] = 0
         then Result := Result xor HashData[ 9, WHITE, KING ];
      if Board.Index[ WQRMoved ] = 0
         then Result := Result xor HashData[ 10, WHITE, KING ];
    end;

 if Board.Index[ BKMoved ] = 0
    then begin
      if Board.Index[ BKRMoved ] = 0
         then Result := Result xor HashData[ 9, BLACK, KING ];
      if Board.Index[ BQRMoved ] = 0
         then Result := Result xor HashData[ 10, BLACK, KING ];
    end;     
end; // InitHashValue

function InitPawnHash : Integer;
         var A : Integer;
begin
  if Board.Side = WHITE
     then Result := 0
     else Result := 1234567890;

  if Board.Index[ WMat ] < END_MAT
     then Result := Result xor $BADC0DE;

  if Board.Index[ BMat ] < END_MAT
     then Result := Result xor $A1EB1DA;

  if IsPawnEnding
     then Result := Result xor $DEADBEE;

  for A := -16 to -8 do
  if  ( Board.List[ A ].Sqr <> OUTSIDE )
  and ( Board.List[ A ].Kind  = PAWN )
      then Result := Result xor HashData[ Board.List[ A ].Sqr, BLACK, Board.List[ A ].Kind];

  for A := 8 to 16 do
  if  ( Board.List[ A ].Sqr  <> OUTSIDE )
  and ( Board.List[ A ].Kind = PAWN )
      then Result := Result xor HashData[ Board.List[ A ].Sqr, WHITE, Board.List[ A ].Kind];

  Result := Result xor HashData[ WhiteKingLoc, WHITE, KING ]
                   xor HashData[ BlackKingLoc, BLACK, KING ];

  if Board.Index[ WMat ] < -12
     then Result := Result xor HashData[ 8, WHITE, KING ];
  if Board.Index[ BMat ] < -12
     then Result := Result xor HashData[ 8, BLACK, KING ];
end; // InitPawnHash

function GetHashAddr( HTemp : Integer ) : Integer;
begin
     Result := HTemp mod HASH_SIZE;
end;  // GetHashAddr

procedure SaveHashData;
begin
     AssignFile( RandFile, 'rand.dat' );
     Rewrite( RandFile );
     Write( RandFile, HashData );
     CloseFile( RandFile );
end; // SaveHashData

procedure LoadHashData;
begin
     Reset( RandFile );
     Read( RandFile, HashData );
     CloseFile( RandFile );
end; // LoadHashData

procedure CreateHashData;
          var A, B, C : Integer;
begin
     for A := 0 to 127 do
     for B := 0 to 1 do
     for C := 1 to 6 do
     HashData[ A, B, C ] := Random( High( Integer ) );
end; // CreateHashData

procedure SetHashData;
begin
     AssignFile( RandFile, 'rand.dat' );
     if FileExists( 'rand.dat' )
     then LoadHashData
     else begin
       CreateHashData;
       SaveHashData;
     end;
end; // SetHashData

procedure WriteHash(HNo, Key : Integer; SqFrom, SqTo, Depth : ShortInt; Val, Alfa, Beta : SmallInt );
begin
     if NoMem then Exit; // search interrupted = invalid result

     if ( MainHash[ HNo ].Key <> Key )
     or ( MainHash[ HNo ].Depth < Depth )
     then begin
        MainHash[ HNo ].Key    := Key;
        MainHash[ HNo ].Depth  := Depth;
        MainHash[ HNo ].SqFrom := SqFrom;
        MainHash[ HNo ].SqTo   := SqTo;
        MainHash[ HNo ].Val    := Val;
        if Val >= Beta
           then MainHash[ HNo ].Flag:= ABOVE_BETA
           else begin
             if Val <= Alfa
                then MainHash[ HNo ].Flag := BELOW_ALFA
                else MainHash[ HNo ].Flag := EXACT;
           end;
     end;
end; // WriteHash
