
procedure SetList;              // Initializing the piece
          var A : Integer;      // list. "Color" won't be
begin                           // changed afterwards.  .
  for A := -16 to 16 do
  with Board do begin           // loop
    if A < 0
       then List[A].Color := BLACK
       else List[A].Color := WHITE;
    List[ A ].Kind := 0;
    List[ A ].Sqr  := OUTSIDE;
  end;                          // loop
  Board.List[ 0 ].Color := -1;  // no piece - no color
end; // SetList

procedure SetPieceSquare;       // Inverting  piece/square
          var A : Integer;      // tables  to  create  the
begin                           // second  set for  Black.
  for A := 0 to 127 do
  begin // loop
    BP[ Dark[A] ] :=  WP[A];     BF[ Dark[A] ] :=  WF[A];
    BN[ Dark[A] ] :=  WN[A];     BW[ Dark[A] ] :=  WW[A];
    BB[ Dark[A] ] :=  WB[A];
    BR[ Dark[A] ] :=  WR[A];
    BRC[ Dark[A] ] := WRC[A];
    BQO[ Dark[A] ] := WQO[A];
    BQE[ Dark[A] ] := WQE[A];
    BK[ Dark[A] ] :=  WK[A];
    BNOutpost[ Dark[A] ] := WNOutpost[A];
    BROutpost[ Dark[A] ] := WROutpost[A];
    BKSStorm[ Dark[A] ]  := WKSStorm[A];
    BQSStorm[ Dark[A] ]  := WKSStorm[A];
    BPBB[ Dark[A] ]      := WPBB[A];
  end; // loop
end; // SetPieceSquare

procedure SetEmptyFiles;        // Setting an auxiliary
          var A, B : Integer;   // data structure, used
begin                           // to   initialize  the
   for A := -1 to 9 do          // data  set concerning
   for B :=  0 to 1 do          // open files.        .
   EmptyFiles[ A, B ] := 0;
end; // SetEmptyFiles

procedure SetArrToSq;
          var A, B, C : Integer;
          Temp : string;
begin
  for A := 0 to 7 do
  for B := 0 to 7 do
  begin
    Temp := ColName[ A ] + RowName[ B ];
    for C := 0 to 127 do
    if SqrName[ C ] = Temp
       then ArrToSq[ A, B ] := C;
  end;
end; // SetArrToSq

procedure PrepareAll;
begin
  ClockSide := WHITE;
  WMTime   := 0;   BMTime   := 0;
  WTotTime := 0;   BTotTime := 0;
  SetList;
  SetPieceSquare;
  SetEmptyFiles;
  SetArrToSq;
end; // PrepareAll
