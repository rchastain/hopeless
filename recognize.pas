
function IsRim( Sqr : Integer ) : Boolean;
begin
  if ( Sqr mod 16 = COL_A ) or ( Sqr mod 16 = COL_H )
  or ( Sqr div 16 = ROW_1 ) or ( Sqr div 16 = ROW_8 )
     then Result := TRUE
     else Result := FALSE;
end; // IsRim

procedure ClaimResult;                   // This  procedure
          var A, Key, NofRep : Integer;  // is called after
begin                                    // every move made
  Key := InitHashValue;                  // on  the  board.
  NofRep := 0;                           // It  checks  if
  for A := 1 to StackIndex -1 do         // the  resulting
      if RepStack[ A ] = Key             // position   is
         then Inc( NofRep );             // a  draw  under
  if NofRep > 3                          // FIDE rules.
    {$IFDEF XBOARD}
    then Form1.WBThread.SendCommand( '1/2-1/2{threefold repetition}', TRUE );
    {$ELSE} then Form1.InfoMessage( sDrawByRep ); {$ENDIF}

  if Board.MovesTo50 > 100
     {$IFDEF XBOARD}
     then Form1.WBThread.SendCommand( '1/2-1/2{fifty moves rule}', TRUE );
     {$ELSE} then Form1.InfoMessage( sDrawBy50m ); {$ENDIF}

  if Board.Index[ WPCount ] + Board.Index[ BPCount ] = 0
     then begin // claim draws by insufficient material
        if Board.Index[ WMat ] = -16 // white has nothing
           then begin
             if Board.Index[BMat] < -14
                {$IFDEF XBOARD}
                then Form1.WBThread.SendCommand('1/2-1/2{insufficient material}', TRUE );
                {$ELSE} then Form1.InfoMessage( sDrawByMat ); {$ENDIF}
             if  ( Board.Index[ BMat ] = -14 )
             and ( Board.Index[ BNCount ] = 2 )
                {$IFDEF XBOARD}
                then Form1.WBThread.SendCommand('1/2-1/2{insufficient material}', TRUE );
                {$ELSE} then Form1.InfoMessage( sDrawByMat ); {$ENDIF}
           end;
        if Board.Index[ BMat ] = -16 // white has nothing
           then begin
              if Board.Index[ WMat ] < -14
                 {$IFDEF XBOARD}
                 then Form1.WBThread.SendCommand('1/2-1/2{insufficient material}', TRUE );
                 {$ELSE} then Form1.InfoMessage( sDrawByMat ); {$ENDIF}
              if  ( Board.Index[ WMat ] = -14 )
              and ( Board.Index[ WNCount ] = 2 )
                 {$IFDEF XBOARD}
                 then Form1.WBThread.SendCommand('1/2-1/2{insufficient material}', TRUE );
                 {$ELSE} then Form1.InfoMessage( sDrawByMat ); {$ENDIF}
           end;
     end; // no pawns

     if MySearch.IsCheckMate
        then begin
            // ProgPassive := TRUE;
             {$IFNDEF XBOARD}
             Form1.InfoMessage( sCheckmate );
             {$ENDIF}
        end;

     if MySearch.IsStaleMate
        then begin
            // ProgPassive := TRUE;
             {$IFNDEF XBOARD}
             Form1.InfoMessage( sStalemate );
             {$ENDIF}
        end;

end; // ClaimResult
                                  // "IsDraw"  identifies
function IsDraw : Boolean;        // positions drawn with
begin                             // best play.
  Result := FALSE;

  if  ( Board.Index[ WMat ] = -16 )
  and ( Board.Index[ BMat ] = -16 )
  then begin                            // draws in pawn endings
    if Board.Index[ WPCount ] = 0
       then begin
      //  1 : bare Kings in any legal position
         if  ( Board.Index[ BPCount ] = 0 )
         and ( not IsAttackedBy( WHITE, BlackKingLoc, TRUE ) )
             then begin Result := TRUE; Exit; end;

         if Board.Index[ BPCount ] = 1    // 2: draws in
            then begin                    // KpK (black's
              if Board.Side = BLACK       // advantage
                 then begin // black to move
                   if  ( IsPiece( BLACK, PAWN, WhiteKingLoc + NORTH ) )
                   and ( IsPiece( BLACK, KING, WhiteKingLoc + NNE ) )
                       then begin Result := TRUE; Exit; end;

                   if  ( IsPiece( BLACK, PAWN, WhiteKingLoc + NORTH ) )
                   and ( IsPiece( BLACK, KING, WhiteKingLoc + NNW ) )
                       then begin Result := TRUE; Exit; end;
                 end
                 else begin // white to move
                   if  ( IsPiece( BLACK, PAWN, WhiteKingLoc + NORTH ) )
                   and ( IsPiece( BLACK, KING, WhiteKingLoc + NN ) )
                       then begin Result := TRUE; Exit; end;
                 end;
            end;
       end
       else begin
        if  ( Board.Index[ BPCount ] = 0 )  // draws in
        and ( Board.Index[ WPCount ] = 1 )  // KpK (White's
        then begin                          // advantage
          if Board.Side = BLACK
          then begin // black to move
            if  ( IsPiece( WHITE, PAWN, BlackKingLoc + SOUTH ) )
            and ( IsPiece( WHITE, KING, BlackKingLoc + SS ) )
                then begin Result := TRUE; Exit; end;
          end
          else begin // white to move
            if  ( IsPiece( WHITE, PAWN, BlackKingLoc + SOUTH ) )
            and ( IsPiece( WHITE, KING, BlackKingLoc + SSE ) )
                then begin Result := TRUE; Exit; end;

            if  ( IsPiece( WHITE, PAWN, BlackKingLoc + SOUTH ) )
            and ( IsPiece( WHITE, KING, BlackKingLoc + SSW ) )
                then begin Result := TRUE; Exit; end;
          end;
        end;
      end; // no pieces
  end;

  if  ( Board.Index[ WPCount ] = 0 )
  and ( Board.Index[ BPCount ] = 0 )
  then begin // no pawns
    if  ( Board.Index[ WMat ] <= -15 )  // K(m) vs K(m)
    and ( Board.Index[ BMat ] <= -15 )
    and ( not IsRim( WhiteKingLoc ) )  // guarding against mate in one
    and ( not IsRim( BlackKingLoc ) )  // when both sides have a piece
    and ( not IsAttackedBy( BLACK, WhiteKingLoc, TRUE ) )
    and ( not IsAttackedBy( WHITE, BlackKingLoc, TRUE ) )
        then begin
          Result := TRUE;
          Exit;
        end;
  end; // no pawns
end; // IsDraw
