
function SafeDiv( ToDiv, DivBy : Integer ) : Integer;
begin
   if DivBy = 0
      then DivBy := 1;
   Result := ToDiv div DivBy;
end; // SafeDiv

function DivString( ToDiv, DivBy : Integer ) : string;
begin
  Result := IntToStr( SafeDiv( ToDiv, DivBy ) );
end; // DivString

procedure SwitchSide;
begin
   Board.Side := Board.Side xor 1;
end; // SwitchSide

function ListSide : Integer;
begin
     if Board.Side = WHITE
        then Result := 1
        else Result := -1;
end; // ListSide

function WhiteKingLoc : Integer;
begin
     Result := Board.List[ 1 ].Sqr;
end; // WhiteKingLoc

function BlackKingLoc : Integer;
begin
     Result := Board.List[ -1 ].Sqr;
end; // BlackKingLoc

function IsSquare( Sqr : Integer ) : Boolean;
begin
   if Sqr and $88 = 0
      then Result := TRUE
      else Result := FALSE;
end; // IsSquare

function IsEmpty( Sqr : Integer ) : Boolean;
begin
  if  ( Sqr and $88 = 0 )
  and ( Board.Index[ Sqr ] = 0 )
      then Result := TRUE
      else Result := FALSE;
end; // IsEmpty

function IsColor( PColor, Sqr : Integer ) : Boolean;
begin
   if  ( Sqr and $88 = 0 )
   and ( Board.List[ Board.Index[Sqr] ].Color = PColor )
       then Result := TRUE
       else Result := FALSE;
end; // IsColor

function IsPiece( PColor, PKind, Sqr : Integer ) : Boolean;
begin
   if  ( Sqr and $88 = 0 )
   and ( Board.List[ Board.Index[Sqr] ].Color = PColor )
   and ( Board.List[ Board.Index[Sqr] ].Kind = PKind )
       then Result := TRUE
       else Result := FALSE;
end; // IsPiece

// Version of IsPiece used in detection of threats by pawns

function IsPieceNC( PKind, Sqr : Integer ) : Boolean;
begin
   if  ( Sqr and $88 = 0 )
   and ( Board.List[ Board.Index[Sqr] ].Kind = PKind )
       then Result := TRUE
       else Result := FALSE;
end; // IsPieceNC

// The  function must be called in the situation  where
// there  is a piece on a square "Sqr". Then  it  tests
// for its color and checks if it's defended by a pawn.

function PawnDefends( Sqr : Integer ) : Boolean;
begin
   Result := FALSE;
   if  ( Board.Index[ Sqr ] < 0 )
   and ( IsPiece( BLACK, PAWN, Sqr + NE ) or IsPiece( BLACK, PAWN, Sqr + NW ) )
       then Result := TRUE;

   if  ( Board.Index[ Sqr ] > 0 )
   and ( IsPiece( WHITE, PAWN, Sqr + SE ) or IsPiece( WHITE, PAWN, Sqr + SW ) )
       then Result := TRUE;
end; // PawnDefends

function GetColumn( Sqr : Integer ) : Integer;
begin
   Result := Sqr mod 16;
end; // GetColumn

function GetRow( Sqr : Integer ) : Integer;
begin
   Result := Sqr div 16;
end; // GetRow

function TwoPawns( PColor, Sqr1, Sqr2 : Integer ) : Boolean;
begin
   if  ( IsPiece( PColor, PAWN, Sqr1 ) )
   and ( IsPiece( PColor, PAWN, Sqr2 ) )
       then Result := TRUE
       else Result := FALSE;
end; // TwoPawns

function IsEnding : Boolean;
begin
  if  ( Board.Index[ WMat ] < -12 )
  and ( Board.Index[ BMat ] < -12 )
      then Result := TRUE
      else Result := FALSE; 
end; // IsEnding

function IsPawnEnding : Boolean;
begin
   if  ( Board.Index[ WMat ] = -16 )
   and ( Board.Index[ BMat ] = -16 )
       then Result := TRUE
       else Result := FALSE;
end; // IsPawnEnding

// Checking if a bishop vector denotes moving forward

function IsWBFwdVect( Vect : Integer ) : Boolean;
begin
  if ( Vect = NE ) or ( Vect = NW )
     then Result := TRUE
     else Result :=FALSE;
end; // IsWBFwdVect

function IsBBFwdVect( Vect : Integer ) : Boolean;
begin
  if ( Vect = SE ) or ( Vect = SW )
     then Result := TRUE
     else Result :=FALSE;
end; // IsWBFwdVect

// Checking if a sliding piece moves along given vector.

function IsSliderVector( Piece, Vect : Integer ) : Boolean;
begin
  Result := FALSE;
  case Piece of
     QUEEN : Result := TRUE;
     ROOK  : if (Vect = NORTH ) or (Vect = SOUTH ) or (Vect = WEST ) or ( Vect = EAST ) then Result := TRUE;
     BISH  : if (Vect = NE) or (Vect = NW) or (Vect = SE) or (Vect = SW ) then Result := TRUE;
  end; // of case
end;   // IsSliderVector

procedure ClearBoard;
          var A : Integer;
begin
  StackIndex := 0;
  Board.Side := WHITE;
  Board.Ep := OUTSIDE;
  for A := 0 to 127 do Board.Index[ A ] := 0;

  for A := -16 to 16 do
  begin
    Board.List[ A ].Kind := 0;
    Board.List[ A ].Sqr  := OUTSIDE;
  end;
end; // ClearBoard
