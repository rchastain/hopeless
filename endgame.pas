
// This file is a part of Clericus Chess project
// by Pawel Koziol, nescitus@o2.pl

//------------------------------------------------------

// The following function tries to define endgame type
// for the benefit of approppriate evaluation changes.

function TEval.EndgameType : TEndgame;
begin
     Result := UNDEFINED;

     if  ( Board.Index[ WMat ] = -16 )
     and ( Board.Index[ BMat ] = -16 )
     then Result := PAWN_END;

     if  ( Board.Index[ WMat ] = -15 )  // This is a minor
     and ( Board.Index[ BMat ] = -15 )  // piece  endgame.
     then begin
       if OppositeBishops
          then Result := BOC_END
          else begin
            Result := BISH_END;
            if  ( Board.Index[ WBCount ] = 0 )
            and ( Board.Index[ BBCount ] = 0 )
            then Result := KNIGHT_END
            else begin
              if Board.Index[ WBCount ] <> Board.Index[ BBCount ]
                 then Result := BN_END;
            end;
          end;

       if  ( Board.Index[ WPCount ] = 1 )
       and ( Board.Index[ BPCount ] = 0 )
       and ( Board.Index[ WBCount ] = 1 )
           then Result := KBPKX;

       if  ( Board.Index[ BPCount ] = 1 )
       and ( Board.Index[ WPCount ] = 0 )
       and ( Board.Index[ BBCount ] = 1 )
           then Result := KBPKX;

     end; // minor piece endgame

     if  ( Board.Index[ WMat ] = -14 )
     and ( Board.Index[ BMat ] = -14 )
     and ( Board.Index[ WRCount ] = 1 )
     and ( Board.Index[ BRCount ] = 1 )
         then begin
           Result := ROOK_END;
           if Board.Index[ WPCount ] + Board.Index[ BPCount ] = 1
              then Result := KRPKR;
         end;
end; // EndgameType

// The next two functions detect dead draws in KRP vs. KR.
// They are split depending on the side possessing a pawn.

function TEval.IsDrawKRPKR_White : Boolean;
begin
   Result := FALSE;

   if  ( IsPiece( WHITE, PAWN, A7 ) )
   and ( IsPiece( WHITE, ROOK, A8 ) )
   and ( IsPiece( BLACK, KING, G7 ) or IsPiece( BLACK, KING, H7 ) )
   and ( IsPiece( BLACK, ROOK, A1 ) or IsPiece( BLACK, ROOK, A2 ) or IsPiece( BLACK, ROOK, A3 ) )
   then begin Result := TRUE; Exit; end;

   if  ( IsPiece( WHITE, PAWN, H7 ) )
   and ( IsPiece( WHITE, ROOK, H8 ) )
   and ( IsPiece( BLACK, KING, B7 ) or IsPiece( BLACK, KING, A7 ) )
   and ( IsPiece( BLACK, ROOK, H1 ) or IsPiece( BLACK, ROOK, H2 ) or IsPiece( BLACK, ROOK, H3 ) )
   then begin Result := TRUE; Exit; end;

end; // IsDrawKRPKR_White

function TEval.IsDrawKRPKR_Black : Boolean;
begin
   Result := FALSE;

   if  ( IsPiece( BLACK, PAWN, H2 ) )
   and ( IsPiece( BLACK, ROOK, H1 ) )
   and ( IsPiece( WHITE, KING, B2 ) or IsPiece( WHITE, KING, A2 ) )
   and ( IsPiece( WHITE, ROOK, H8 ) or IsPiece( WHITE, ROOK, H7 ) or IsPiece( WHITE, ROOK, H6 ) )
   then begin Result := TRUE; Exit; end;

   if  ( IsPiece( BLACK, PAWN, A2 ) )
   and ( IsPiece( BLACK, ROOK, A1 ) )
   and ( IsPiece( WHITE, KING, G2 ) or IsPiece( WHITE, KING, H2 ) )
   and ( IsPiece( WHITE, ROOK, A8 ) or IsPiece( WHITE, ROOK, A7 ) or IsPiece( WHITE, ROOK, A6 ) )
   then begin Result := TRUE; Exit; end;

end; // IsDrawKRPKR_Black

// The  following function determines if an endgame  where
// one  side has a bishop and a pawn versus a minor  piece
// can  be  considered drawn. It uses a  simple  heuristic
// borrowed  from Glaurung 1.2.1 : if a king of  a  weaker
// side  stands on pawn's way on a square that  cannot  be
// attacked  by enemy bishop, then it's a draw. Glaurung 2
// uses more elaborate conditions though.

function TEval.IsDrawKBPKX ( CurrScore : Integer ) : Boolean;
begin
  Result := FALSE;

  if CurrScore > 0  // white advantage
     then begin
       if  ( IsOnWPWay( BlackKingLoc ) )
       and ( ( SqrCol[ BlackKingLoc ] = WHITE ) and ( IsBishop(5) ) or ( SqrCol[ BlackKingLoc ] = BLACK ) and ( IsBishop(6) ) )
           then Result := TRUE;
     end;

  if CurrScore < 0  // black advantage
     then begin
       if  ( IsOnBPWay( WhiteKingLoc ) )
       and ( ( SqrCol[ WhiteKingLoc ] = WHITE ) and ( IsBishop(-5) ) or ( SqrCol[ WhiteKingLoc ] = BLACK ) and ( IsBishop(-6) ) )
           then Result := TRUE;
     end;
end; // IsDrawKBPKX

// There follows a function looking for low material draws:
//
// 1. a single minor cannot win
// 2. two knights cannot win against a single minor
// 3. rook pawn(s) don't win if opponent controls
//    promotion square
// 4. KBP(p) vs K (rook pawn(s) and bad bishop ) draws
//    if the weaker side controls promotion square
// 5. there is a rare draw with rook pawn(s) and
//    theoretically good bishop against a king and a pawn,
//    provided that a pawn of the weaker side is blocked
//    by opponent's pawn on its initial square.

function TEval.IsInsufficientMaterial( CurrScore : Integer ) : Boolean;

begin
   Result := FALSE;

   if  ( Board.Index[ WPCount ] = 0 )
   and ( Board.Index[ WMat ] = -15  )
   then begin              // white has a single minor

     if CurrScore > 0                    // 1. a single minor
        then Result := TRUE;             // cannot win

     if  ( Board.Index[ BPCount ] = 0 )  // 2. two bare kniqhts
     and ( Board.Index[ BMat ] = -14  )  // cannot win
     and ( Board.Index[ BNCount ] = 2 )  // against a minor
         then Result := TRUE;

   end; // white has a single minor

   if  ( Board.Index[ BPCount ] = 0 )
   and ( Board.Index[ BMat ] = -15 )
   then begin // black has a single minor

      if CurrScore < 0
         then Result := TRUE;

      if  ( Board.Index[ WPCount ] = 0 )
      and ( Board.Index[ WMat ] = -14  )
      and ( Board.Index[ WNCount ] = 2 )
         then Result := TRUE;

   end; // black has a single minor

   // 3. rook pawn(s)

   if  ( Board.Index[ WMat ] = -16 )
   and ( Board.Index[ BMat ] = -16 )
   then begin

   if  ( Board.Index[ WPCount ] = 0 )
   and ( Board.Index[ BPCount ] > 0 )
   then begin
        if  ( FileStat[ COL_A, BLACK ] = Board.Index[ BPCount ] )
        and ( ( WhiteKingLoc = A1 ) or ( WhiteKingLoc = B1 ) or ( WhiteKingLoc = A2 ) or ( WhiteKingLoc = B2 ) )
            then Result := TRUE;

        if  ( FileStat[ COL_H, BLACK ] = Board.Index[ BPCount ] )
        and ( ( WhiteKingLoc = H1 ) or ( WhiteKingLoc = G1 ) or ( WhiteKingLoc = H2 ) or ( WhiteKingLoc = G2 ) )
            then Result := TRUE;
   end;

   if  ( Board.Index[ BPCount ] = 0 )
   and ( Board.Index[ WPCount ] > 0 )
   then begin
        if  ( FileStat[ COL_A, WHITE ] = Board.Index[ WPCount ] )
        and ( ( BlackKingLoc = A8 ) or ( BlackKingLoc = B8 ) or ( BlackKingLoc = A7 ) or ( BlackKingLoc = B7 ) )
            then Result := TRUE;

        if  ( FileStat[ COL_H, WHITE ] = Board.Index[ WPCount ] )
        and ( ( BlackKingLoc = H8 ) or ( BlackKingLoc = G8 ) or ( BlackKingLoc = H7 ) or ( BlackKingLoc = G7 ) )
            then Result := TRUE;
   end;

   end; // rook pawns

   // 4. rook pawn(s) and bad bishop against a bare king

   if  ( Board.Index[ WMat ] = -16 )   // black advantage
   and ( Board.Index[ BMat ] = -15 )
   and ( CurrScore < 0 )
   and ( Board.Index[ BPCount ] > 0 )
   then begin
     if  ( FileStat[ COL_A, BLACK ] = Board.Index[ BPCount ] )
     and ( IsBishop( -6 ) )
     and ( ( WhiteKingLoc = A1 ) or ( WhiteKingLoc = B1 ) or ( WhiteKingLoc = A2 ) or ( WhiteKingLoc = B2 ) )
         then Result := TRUE;

     if  ( FileStat[ COL_H, BLACK ] = Board.Index[ BPCount ] )
     and ( IsBishop( -5 ) )
     and ( ( WhiteKingLoc = H1 ) or ( WhiteKingLoc = G1 ) or ( WhiteKingLoc = H2 ) or ( WhiteKingLoc = G2 ) )
         then Result := TRUE;
   end;

   if  ( Board.Index[ BMat ] = -16 )  // white advantage
   and ( Board.Index[ WMat ] = -15 )
   and ( CurrScore > 0 )
   and ( Board.Index[ WPCount ] > 0 )
   then begin
     if  ( FileStat[ COL_A, WHITE ] = Board.Index[ WPCount ] )
     and ( IsBishop( 5 ) )
     and ( ( BlackKingLoc = A8 ) or ( BlackKingLoc = B8 ) or ( BlackKingLoc = A7 ) or ( BlackKingLoc = B7 ) )
         then Result := TRUE;

     if  ( FileStat[ COL_H, WHITE ] = Board.Index[ WPCount ] )
     and ( IsBishop( 6 ) )
     and ( ( BlackKingLoc = H8 ) or ( BlackKingLoc = G8 ) or ( BlackKingLoc = H7 ) or ( BlackKingLoc = G7 ) )
         then Result := TRUE;
   end;

   // 5. a rare draw with rook pawn(s) and theoretically good
   // bishop against a king and a pawn

   if  ( Board.Index[ WMat ] = -16 )   // black advantage
   and ( Board.Index[ BMat ] = -15 )
   and ( CurrScore < 0 )
   and ( Board.Index[ BPCount ] > 0 )
   then begin

     if  ( FileStat[ COL_A, BLACK ] = Board.Index[ BPCount ] )
     and ( IsPiece( WHITE, PAWN, A2 ) )
     and ( IsPiece( BLACK, PAWN, A3 ) )
     and ( IsBishop( -5 ) )
     and ( ( WhiteKingLoc = A1 ) or ( WhiteKingLoc = B1 ) or ( WhiteKingLoc = C2 ) or ( WhiteKingLoc = B2 ) )
         then Result := TRUE;

     if  ( FileStat[ COL_H, BLACK ] = Board.Index[ BPCount ] )
     and ( IsPiece( WHITE, PAWN, H2 ) )
     and ( IsPiece( BLACK, PAWN, H3 ) )
     and ( IsBishop( -6 ) )
     and ( ( WhiteKingLoc = H1 ) or ( WhiteKingLoc = G1 ) or ( WhiteKingLoc = F2 ) or ( WhiteKingLoc = G2 ) )
         then Result := TRUE;
   end;  // black advantage

   if  ( Board.Index[ BMat ] = -16 )  // white advantage
   and ( Board.Index[ WMat ] = -15 )
   and ( CurrScore > 0 )
   and ( Board.Index[ WPCount ] > 0 )
   then begin
     if  ( FileStat[ COL_A, WHITE ] = Board.Index[ WPCount ] )
     and ( IsPiece( BLACK, PAWN, A7 ) )
     and ( IsPiece( WHITE, PAWN, A6 ) )
     and ( IsBishop( 6 ) )
     and ( ( BlackKingLoc = A8 ) or ( BlackKingLoc = B8 ) or ( BlackKingLoc = C7 ) or ( BlackKingLoc = B7 ) )
         then Result := TRUE;

     if  ( FileStat[ COL_H, WHITE ] = Board.Index[ WPCount ] )
     and ( IsPiece( BLACK, PAWN, H7 ) )
     and ( IsPiece( WHITE, PAWN, H6 ) )
     and ( IsBishop( 5 ) )
     and ( ( BlackKingLoc = H8 ) or ( BlackKingLoc = G8 ) or ( BlackKingLoc = F7 ) or ( BlackKingLoc = G7 ) )
         then Result := TRUE;
   end; // white advantage

   // 6. the same as 5, only at the "b" and "g" files

   if  ( Board.Index[ WMat ] = -16 )   // black advantage
   and ( Board.Index[ BMat ] = -15 )
   and ( CurrScore < 0 )
   and ( Board.Index[ BPCount ] > 0 )
   then begin

     if  ( FileStat[ COL_B, BLACK ] = Board.Index[ BPCount ] )
     and ( IsPiece( WHITE, PAWN, B2 ) )
     and ( IsPiece( BLACK, PAWN, B3 ) )
     and ( IsBishop( -6 ) )
     and ( ( WhiteKingLoc = A1 ) or ( WhiteKingLoc = B1 ) or ( WhiteKingLoc = C1 ) or ( WhiteKingLoc = D2 ) )
         then Result := TRUE;

     if  ( FileStat[ COL_G, BLACK ] = Board.Index[ BPCount ] )
     and ( IsPiece( WHITE, PAWN, G2 ) )
     and ( IsPiece( BLACK, PAWN, G3 ) )
     and ( IsBishop( -5 ) )
     and ( ( WhiteKingLoc = H1 ) or ( WhiteKingLoc = G1 ) or ( WhiteKingLoc = F1 ) or ( WhiteKingLoc = E2 ) )
         then Result := TRUE;
   end;  // black advantage

   if  ( Board.Index[ BMat ] = -16 )  // white advantage
   and ( Board.Index[ WMat ] = -15 )
   and ( CurrScore > 0 )
   and ( Board.Index[ WPCount ] > 0 )
   then begin
     if  ( FileStat[ COL_B, WHITE ] = Board.Index[ WPCount ] )
     and ( IsPiece( BLACK, PAWN, B7 ) )
     and ( IsPiece( WHITE, PAWN, B6 ) )
     and ( IsBishop( 5 ) )
     and ( ( BlackKingLoc = A8 ) or ( BlackKingLoc = B8 ) or ( BlackKingLoc = C8 ) or ( BlackKingLoc = D7 ) )
         then Result := TRUE;

     if  ( FileStat[ COL_G, WHITE ] = Board.Index[ WPCount ] )
     and ( IsPiece( BLACK, PAWN, G7 ) )
     and ( IsPiece( WHITE, PAWN, G6 ) )
     and ( IsBishop( 6 ) )
     and ( ( BlackKingLoc = H8 ) or ( BlackKingLoc = G8 ) or ( BlackKingLoc = F8 ) or ( BlackKingLoc = F7 ) )
         then Result := TRUE;
   end; // white advantage

   // end of insufficient material draws

end; // IsInsufficientMatrial