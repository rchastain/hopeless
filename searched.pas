unit searched;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, language;

type
  TSearchEditor = class(TForm)
    Fut1Box: TCheckBox;
    Fut2Box: TCheckBox;
    RazorBox: TCheckBox;
    Fut1Edit: TSpinEdit;
    Fut2Edit: TSpinEdit;
    RazorEdit: TSpinEdit;
    PVSBox: TCheckBox;
    AspEdit: TSpinEdit;
    IIDBox: TCheckBox;
    LMRBox: TCheckBox;
    StaticBox: TCheckBox;
    AspLabel: TLabel;
    HashPVBox: TCheckBox;
    procedure RazorBoxClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PVSBoxClick(Sender: TObject);
    procedure IIDBoxClick(Sender: TObject);
    procedure LMRBoxClick(Sender: TObject);
    procedure StaticBoxClick(Sender: TObject);
    procedure Fut1BoxClick(Sender: TObject);
    procedure Fut2BoxClick(Sender: TObject);
    procedure AspEditChange(Sender: TObject);
    procedure Fut1EditChange(Sender: TObject);
    procedure Fut2EditChange(Sender: TObject);
    procedure RazorEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HashPVBoxClick(Sender: TObject);
  private

  public
    procedure PolishLanguage;
    procedure EnglishLanguage;
    procedure LanguageSettings;
  end;

var
  SearchEditor: TSearchEditor;

implementation

uses main;

{$R *.dfm}

procedure TSearchEditor.PolishLanguage;
begin
  AspLabel.Caption := 'Okno oczekiwa�';
  StaticBox.Caption := 'Przycinanie statyczne';
  HashPVBox.Caption := 'Odci�cia z TT w PV';
end; // PolishLanguage

procedure TSearchEditor.EnglishLanguage;
begin
  AspLabel.Caption := 'Aspiration window';
  StaticBox.Caption := 'Static pruning';
  HashPVBox.Caption := 'Hash in PV';
end; // EnglishLanguage

procedure TSearchEditor.LanguageSettings;
begin
  case vLang of
    POLISH:
      PolishLanguage;
    ENGLISH:
      EnglishLanguage;
  end; // of case
end; // LanguageSettings

procedure TSearchEditor.RazorBoxClick(Sender: TObject);
begin
  // TODO: delete this box
  //MySearch.DO_RAZOR := RazorBox.Checked;
end;

procedure TSearchEditor.FormShow(Sender: TObject);
begin
  PVSBox.Checked := MySearch.DO_PVS;
  IIDBox.Checked := MySearch.DO_IID;
  LMRBox.Checked := MySearch.DO_LMR;
  StaticBox.Checked := MySearch.DO_STATIC;
  Fut1Box.Checked := MySearch.DO_FUT1;
  Fut2Box.Checked := MySearch.DO_FUT2;
  RazorBox.Checked := FALSE; // TODO: remove
  HashPVBox.Checked := MySearch.HASH_IN_PV;
  AspEdit.Value := MySearch.ASP;
  Fut1Edit.Value := MySearch.FUT_1;
  Fut2Edit.Value := MySearch.FUT_2;
  RazorEdit.Value := 0; // MySearch.RAZOR_LIMIT;  TODO: remove
end;

procedure TSearchEditor.PVSBoxClick(Sender: TObject);
begin
  MySearch.DO_PVS := PVSBox.Checked;
end;

procedure TSearchEditor.IIDBoxClick(Sender: TObject);
begin
  MySearch.DO_IID := IIDBox.Checked;
end;

procedure TSearchEditor.LMRBoxClick(Sender: TObject);
begin
  MySearch.DO_LMR := LMRBox.Checked;
end;

procedure TSearchEditor.StaticBoxClick(Sender: TObject);
begin
  MySearch.DO_STATIC := StaticBox.Checked;
end;

procedure TSearchEditor.Fut1BoxClick(Sender: TObject);
begin
  MySearch.DO_FUT1 := Fut1Box.Checked;
end;

procedure TSearchEditor.Fut2BoxClick(Sender: TObject);
begin
  MySearch.DO_FUT2 := Fut2Box.Checked;
end;

procedure TSearchEditor.AspEditChange(Sender: TObject);
begin
  MySearch.ASP := AspEdit.Value;
end;

procedure TSearchEditor.Fut1EditChange(Sender: TObject);
begin
  MySearch.FUT_1 := Fut1Edit.Value;
end;

procedure TSearchEditor.Fut2EditChange(Sender: TObject);
begin
  MySearch.FUT_2 := Fut2Edit.Value;
end;

procedure TSearchEditor.RazorEditChange(Sender: TObject);
begin
  // TODO: remove
end;

procedure TSearchEditor.FormCreate(Sender: TObject);
begin
  Top := 1;
  Left := form1.Left + form1.Width + 1;
  LanguageSettings;
end;

procedure TSearchEditor.HashPVBoxClick(Sender: TObject);
begin
  MySearch.HASH_IN_PV := HashPVBox.Checked;
end;

end.
