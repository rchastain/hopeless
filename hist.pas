
constructor THistory.Create;
begin
  inherited Create;
  ClearTable;
end; // Create

procedure THistory.ClearTable;
          var A, B : Integer;
begin
  for A := 0 to 127 do
  for B := 0 to 127 do
  begin
     Table[ A, B ] := 0;
  end;
end; // Clear

procedure THistory.Age;
          var A, B : Integer;
begin
  for A := 0 to 127 do
  for B := 0 to 127 do
  begin
     Table[ A, B ] := ( Table[ A, B ] div 5 ) * 4;
  end;
end; // Age

procedure THistory.Trim;
          var A, B : Integer;
begin
  for A := 0 to 127 do
  for B := 0 to 127 do
  begin
     Table[ A, B ] := Table[ A, B ] div 2;
  end;
end; // Trim

procedure THistory.Update( Depth, SqFr, SqTo : Integer);
begin
  if Depth > 0
     then Table[ SqFr, SqTo ] := Table[ SqFr, SqTo ]
                               + Depth * Depth;

  if Table[ SqFr, SqTo ] > 16383 // 2 ^ 14
     then Trim;

end; // UpdateHistoryTable

function THistory.Value( SqFr, SqTo : Integer ) : Integer;
begin
  Result := Table[ SqFr, SqTo ];
end; // Value