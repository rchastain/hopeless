
// This file contains functions dealing with pawn evaluation

// Pawn shield evaluation may  seem too simplistic, but it's
// good enough for this program. If a pawn is near enough to
// its own king, we assign a bonus (PROT_PAWN); A pawn which
// is  too  advanced gets no bonus, and  with  no  pawns  on
// a file near the king we impose a penalty (NO_PROT_PAWN).

function TEval.WKPawnShield( Sqr : Integer ) : Integer;
begin
   Result := 0;

   if ( IsPiece( WHITE, PAWN, Sqr + NORTH ) )
   or ( IsPiece( WHITE, PAWN, Sqr + NN ) )
      then Result := Result + P_PROTECTS_K
      else begin
        if FileStat[ Sqr mod 16, WHITE ] = 0
           then Result := Result + P_DESERTED_K;
      end;

   if ( IsPiece( WHITE, PAWN, Sqr + NE ) )
   or ( IsPiece( WHITE, PAWN, Sqr + NNE ) )
   or ( IsPiece( WHITE, PAWN, Sqr + EAST ) )
      then Result := Result + P_PROTECTS_K
      else begin
        if  ( IsSquare( Sqr + EAST ) )
        and ( FileStat[ ( Sqr + EAST ) mod 16, WHITE ] = 0 )
            then Result := Result + P_DESERTED_K;
      end;

   if ( IsPiece( WHITE, PAWN, Sqr + NW ) )
   or ( IsPiece( WHITE, PAWN, Sqr + NNW ) )
   or ( IsPiece( WHITE, PAWN, Sqr + WEST ) )
      then Result := Result + P_PROTECTS_K
      else begin
        if  ( IsSquare( Sqr + WEST ) )
        and ( FileStat[ ( Sqr + WEST ) mod 16, WHITE ] = 0 )
            then Result := Result + P_DESERTED_K;
      end;
   Result := ( Result * WhiteShield ) div 100;
end; // WKPawnShield

function TEval.BKPawnShield( Sqr : Integer ) : Integer;
begin
   Result := 0;

   if ( IsPiece( BLACK, PAWN, Sqr + SOUTH) )
   or ( IsPiece( BLACK, PAWN, Sqr + SS ) )
      then Result := Result + P_PROTECTS_K
      else begin
        if FileStat[ Sqr mod 16, BLACK ] = 0
           then Result := Result + P_DESERTED_K;
      end;

   if ( IsPiece( BLACK, PAWN, Sqr + SE) )
   or ( IsPiece( BLACK, PAWN, Sqr + SSE ) )
   or ( IsPiece( BLACK, PAWN, Sqr + EAST ) )
      then Result := Result + P_PROTECTS_K
      else begin
        if  ( IsSquare( Sqr + EAST ) )
        and ( FileStat[ ( Sqr + EAST ) mod 16, BLACK ] = 0 )
            then Result := Result + P_DESERTED_K;
      end;

   if ( IsPiece( BLACK, PAWN, Sqr + SW) )
   or ( IsPiece( BLACK, PAWN, Sqr + SSW ) )
   or ( IsPiece( BLACK, PAWN, Sqr + WEST ) )
      then Result := Result + P_PROTECTS_K
      else begin
        if  ( IsSquare( Sqr + WEST ) )
        and ( FileStat[ ( Sqr + WEST ) mod 16, BLACK ] = 0 )
            then Result := Result + P_DESERTED_K;
      end;

   Result := ( Result * BlackShield ) div 100;
end; // BKPawnShield

// Passed pawn detection

function TEval.IsWPFree( Sqr : Integer ) : Boolean;
         var Here : Integer;
begin
   Result := TRUE;
   Here := Sqr + NORTH;

   while ( Here and $88 = 0 ) do
   begin // loop
     if Board.List[ Board.Index[ Here ] ].Kind = PAWN
        then begin
           Result := FALSE;
           Exit;
        end;

     if ( IsPiece( BLACK, PAWN, Here + EAST ) )
     or ( IsPiece( BLACK, PAWN, Here + WEST ) )
        then begin
           Result := FALSE;
           Exit;
        end;

     Here := Here + NORTH;
   end; // loop
end; // IsWPFree

function TEval.IsBPFree( Sqr : Integer ) : Boolean;
         var Here : Integer;
begin
  Result := TRUE;
  Here := Sqr + SOUTH;

  while ( Here and $88 = 0 ) do
  begin  // loop
    if Board.List[ Board.Index[ Here ] ].Kind = PAWN
       then begin
          Result := FALSE;
          Exit;
       end;

    if ( IsPiece( WHITE, PAWN, Here + EAST ) )
    or ( IsPiece( WHITE, PAWN, Here + WEST ) )
       then begin
          Result := FALSE;
          Exit;
       end;

    Here := Here + SOUTH;
  end; // loop
end; // IsBPFree

// Weak pawn detection. Actually, isolated and backward    /
// pawns are evaluated in the same manner, but detection   /
// is split for the sake of readability.                   /

function TEval.IsWPIsolated( Sqr : Integer ) : Boolean;
begin
  if  ( FileStat[ ( Sqr mod 16 ) + 1, WHITE ] = 0 )
  and ( FileStat[ ( Sqr mod 16 ) - 1, WHITE ] = 0 )
      then Result := TRUE
      else Result := FALSE;
end; // IsWPIsolated

function TEval.IsBPIsolated( Sqr : Integer ) : Boolean;
begin
  if  ( FileStat[ ( Sqr mod 16 ) + 1, BLACK ] = 0 )
  and ( FileStat[ ( Sqr mod 16 ) - 1, BLACK ] = 0 )
      then Result := TRUE
      else Result := FALSE;
end; // IsBPIsolated

function TEval.IsWPBackward( Sqr : Integer ) : Boolean;
         var Fin : Integer;
begin
   Result := TRUE;
   Fin := Sqr;
   while Fin and $88 = 0 do
   begin // loop
      if ( IsPiece( WHITE, PAWN, Fin + EAST ) )
      or ( IsPiece( WHITE, PAWN, Fin + WEST ) )
         then begin
           Result := FALSE;
           Exit;
         end;
      Fin := Fin + SOUTH;
     end; // loop
end; // IsWPBackward

function TEval.IsBPBackward( Sqr : Integer ) : Boolean;
         var Fin : Integer;
begin
   Result := TRUE;
   Fin := Sqr;
   while Fin and $88 = 0 do
   begin // loop
      if ( IsPiece( BLACK, PAWN, Fin + EAST ) )
      or ( IsPiece( BLACK, PAWN, Fin + WEST ) )
         then begin
           Result := FALSE;
           Exit;
         end;
      Fin := Fin + NORTH;
   end; // loop
end; // IsBPBackward

// Evaluation of a passed pawn:                            /
//                                                         /
// 1. Adding the piece/square table value.                 /
// 2. Multiplying it by 1.2 if a passer is supported       /
//    by another pawn.                                     /
// 3. Increasing it in the endgame if the opponent's King  /
//    is far from the pawn.                                /
// 4. Checking for the unstoppable passers                 /
//    in pawn endings.                                     /

function TEval.WFreePEval( Sqr : Integer ) : Integer;
         var PromDist, KingDist : Integer;
begin
   if IsWPFree( Sqr )
      then begin
{1}     Result := WF[ Sqr ];

        if ( IsPiece( WHITE, PAWN, Sqr + WEST ) )
        or ( IsPiece( WHITE, PAWN, Sqr + EAST ) )
        or ( IsPiece( WHITE, PAWN, Sqr + SE ) )
        or ( IsPiece( WHITE, PAWN, Sqr + SW ) )
{2}        then Result := ( Result * 5 ) div 4;

        if  ( Board.Index[ WMat ] < END_MAT )
        and ( Board.Index[ BMat ] < END_MAT )
            then begin // endgame
{3}           Result := Result - Dist[ Sqr, BlackKingLoc ];

{4}        if IsPawnEnding
              then begin
                 PromDist := ROW_8 - ( Sqr div 16 );
                 KingDist := Max( HorDist( Sqr, BlackKingLoc ), ROW_8 - ( BlackKingLoc div 16 ) );

                 if Board.Side = BLACK
                    then Dec( KingDist );

                 if  ( PromDist < KingDist )
                 and ( PromDist < WPromDist )
                     then WPromDist := PromDist;
            end; // pawn ending
        end;  // endgame

      end
      else Result := 0;
end; // WFPEval

function TEval.BFreePEval( Sqr : Integer ) : Integer;
         var PromDist, KingDist : Integer;
begin
   if IsBPFree( Sqr )
      then begin
{1}     Result := BF[ Sqr ];

        if ( IsPiece( WHITE, PAWN, Sqr + WEST ) )
        or ( IsPiece( WHITE, PAWN, Sqr + EAST ) )
        or ( IsPiece( WHITE, PAWN, Sqr + NE ) )
        or ( IsPiece( WHITE, PAWN, Sqr + NW ) )
{2}        then Result := ( Result * 5 ) div 4;

        if  ( Board.Index[ WMat ] < END_MAT )
        and ( Board.Index[ BMat ] < END_MAT )
            then begin // endgame
{3}           Result := Result - Dist[ Sqr, WhiteKingLoc ];

{4}           if IsPawnEnding
              then begin
                 PromDist := 8 - ( ROW_8 - ( Sqr div 16 ) );
                 KingDist := Max ( HorDist( Sqr, WhiteKingLoc ), 8 - ( ROW_8 - ( WhiteKingLoc div 16 ) ) );

                 if Board.Side = WHITE
                    then Dec( KingDist );

                 if  ( PromDist < KingDist )
                 and ( PromDist < BPromDist )
                     then BPromDist := PromDist;

              end; // pawn ending

            end; // endgame
      end
      else Result := 0;
end; // BFPEval

// Weak pawn evaluation:                                   /
//                                                         /
// 1. A penalty form a piece/square table                  /
// 2. Further penalty if a pawn is on a half-open file     /
// 3. Alleviation of a penalty in case of a pawn ram       /
// 4. Endgame evaluation - king distance                   /

function TEval.WWeakPEval( Sqr : Integer ) : Integer;
begin
   if ( IsWPIsolated( Sqr ) )
   or ( IsWPBackward( Sqr ) )
      then begin
{1}     Result := P_WEAK + WW[ Sqr ];

{2}     if FileStat[ Sqr mod 16, WHITE ] = 0
           then Result := Result - 5;

{3}     if IsPiece( BLACK, PAWN, Sqr + NORTH )
           then Result := Result + 5;

{4}     if  ( Board.Index[ WMat ] < END_MAT )
        and ( Board.Index[ BMat ] < END_MAT )
            then Result := Result - Dist[ Sqr, WhiteKingLoc ];
      end
      else Result := 0;
end; // WWeakPEval

function TEval.BWeakPEval( Sqr : Integer ) : Integer;
begin
   if ( IsBPIsolated( Sqr ) )
   or ( IsBPBackward( Sqr ) )
      then begin
{1}     Result := P_WEAK + BW[ Sqr ];

{2}     if FileStat[ Sqr mod 16, WHITE ] = 0
           then Result := Result - 5;

{3}     if IsPiece( WHITE, PAWN, Sqr + SOUTH )
           then Result := Result + 5;

{4}     if  ( Board.Index[ WMat ] < END_MAT )
        and ( Board.Index[ BMat ] < END_MAT )
            then Result := Result - Dist[ Sqr, WhiteKingLoc ];

      end
      else Result := 0;
end; // BWeakPEval

// Material value of a pawn depends upon a file - central  /
// pawns are considered more valuable, whereas rook pawns, /
// which can capture only in one direction, are considered /
// slightly weaker due to their poorer mobility.           /

function TEval.PawnMaterial( Sqr : Integer ) : Integer;
begin
   case Sqr mod 16 of
     COL_A : Result := P_VALUE_AH;
     COL_H : Result := P_VALUE_AH;
     COL_D : Result := P_VALUE_DE;
     COL_E : Result := P_VALUE_DE;
     else Result := P_VALUE_N;
   end; // of case
end; // PawnMaterial

function TEval.WPEv( Sqr : Integer ) : Integer;
begin
   Result := WP[ Sqr ]            // 1. A piece/square value
           + WFreePEval( Sqr )    // 2. Possibly a bonus for a passed pawn.
           + WWeakPEval( Sqr )    // 3. Possibly a penalty for a weak pawn.
           + PawnMaterial( Sqr ); // 4. material value by file.

   if Board.Index[ WMat ] > END_MAT
      then begin
        if  ( IsKingSide( WhiteKingLoc ) )
        and ( IsQueenSide( BlackKingLoc ) )
        then Result := Result + WQSStorm[ Sqr ];

        if  ( IsQueenSide( WhiteKingLoc ) )
        and ( IsKingSide( BlackKingLoc ) )
        then Result := Result + WKSStorm[ Sqr ];

      end;

   // 6. Pawn center evaluation.
   case Sqr of
   D4: begin
        if IsPiece( WHITE, PAWN, C4 )
           then Result := Result + 10;
        if IsPiece( WHITE, PAWN, E4 )
           then Result := Result + 15;
        if IsPiece( WHITE, PAWN, E3 )
           then Result := Result + 5;
        if IsPiece( WHITE, PAWN, C3 )
           then Result := Result + 5;
       end; // D4

   E4 : if IsPiece( WHITE, PAWN, D2 )
           then Result := Result - 5;

   E2 : if IsPiece( WHITE, PAWN, D2 )
           then Result := Result - 10;
   end; // of case
end; // WPEv

function TEval.BPEv( Sqr : Integer ) : Integer;
begin
   Result := BP[ Sqr ]
           + BFreePEval( Sqr )
           + BWeakPEval( Sqr )
           + PawnMaterial( Sqr );

   if Board.Index[ BMat ] > END_MAT
      then begin
        if  ( IsKingSide( BlackKingLoc ) )
        and ( IsQueenSide( WhiteKingLoc ) )
        then Result := Result + BQSStorm[ Sqr ];

        if  ( IsQueenSide( BlackKingLoc ) )
        and ( IsKingSide( WhiteKingLoc ) )
        then Result := Result + BKSStorm[ Sqr ];
      end;

   case Sqr of
     D5 : begin
        if IsPiece( BLACK, PAWN, C5 )
           then Result := Result + 10;
        if IsPiece( BLACK, PAWN, E5 )
           then Result := Result + 15;
        if IsPiece( BLACK, PAWN, E6 )
           then Result := Result + 5;
        if IsPiece( BLACK, PAWN, C6 )
           then Result := Result + 5;
     end; // D5

     E5 : if IsPiece( BLACK, PAWN, D7 )
             then Result := Result - 5;

     E7 : if IsPiece( BLACK, PAWN, D7 )
             then Result := Result - 10;
   end; // of case
end; // BPEv

function TEval.PawnEval : Integer;
         var A, HTemp, HNo  : Integer;
begin
   HTemp := InitPawnHash;
   HNo   := GetHashAddr( HTemp );
   if PawnHash[ HNo ].Key = HTemp
      then begin
        Result := PawnHash[ HNo ].Val;  // Retrieve result
        Exit;                           // from  PawnHash.
      end;

   Result    := 0;
   WPromDist := NO_PROM;
   BPromDist := NO_PROM;

   for A := 9 to 16 do
   if  ( Board.List[ A ].Sqr <> OUTSIDE )
   and ( Board.List[ A ].Kind = PAWN )
   then Result := Result + WPEv( Board.List[ A ].Sqr );

   for A := -16 to -8 do
   if  ( Board.List[ A ].Sqr <> OUTSIDE )
   and ( Board.List[ A ].Kind = PAWN )
   then Result := Result - BPEv( Board.List[ A ].Sqr );

   for A := 0 to 7 do                     // Assess doubled
   begin                                  // pawns.  Note
     if FileStat[ A, WHITE ] > 1          // that file sta-
        then Result := Result + P_DOUBLE; // tus  must  be
     if FileStat[ A, BLACK ] > 1          // defined before
        then Result := Result - P_DOUBLE; // running PawnEval
     if FileStat[ A, WHITE ] > 2
        then Result := Result + 2 * P_DOUBLE;  // tripled
     if FileStat[ A, BLACK ] > 2               // pawns are
        then Result := Result - 2 * P_DOUBLE;  // much worse
   end;

   // basic King safety components - PCSQ and pawn shield
   if Board.Index[ BMat ] >= END_MAT
      then Result := Result
                   + WK[ WhiteKingLoc ]
                   + WKPawnShield( WhiteKingLoc );

   if Board.Index[ BMat ] >= END_MAT
      then Result := Result
                   - BK[ BlackKingLoc ]
                   - BKPawnShield( BlackKingLoc );

// Evaluate pawn races - currently this is only very crude
// approximation.

   if IsPawnEnding
      then begin
        if ( WPromDist <> NO_PROM )
           then begin
             if ( BPromDist = NO_PROM )
             or ( WPromDist + 1 < BPromDist )
                then Result := Result + P_RUNNING;
           end;

        if ( BPromDist <> NO_PROM )
           then begin
             if ( WPromDist = NO_PROM )
             or ( BPromDist + 1 < WPromDist )
                then Result := Result - P_RUNNING;
           end;
      end; // pawn ending

   PawnHash[ HNo ].Key := HTemp;
   PawnHash[ HNo ].Val := Result;
end; // PawnEval
