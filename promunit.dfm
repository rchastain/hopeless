object PromForm: TPromForm
  Left = 515
  Top = 107
  Caption = 'PromForm'
  ClientHeight = 37
  ClientWidth = 137
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object QButt: TSpeedButton
    Left = 8
    Top = 8
    Width = 33
    Height = 33
    Caption = 'q'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    OnClick = QButtClick
  end
  object RButt: TSpeedButton
    Left = 40
    Top = 8
    Width = 33
    Height = 33
    Caption = 'r'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    OnClick = RButtClick
  end
  object BButt: TSpeedButton
    Left = 72
    Top = 8
    Width = 33
    Height = 33
    Caption = 'b'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    OnClick = BButtClick
  end
  object NButt: TSpeedButton
    Left = 104
    Top = 8
    Width = 33
    Height = 33
    Caption = 'n'
    Font.Charset = SYMBOL_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Chess Merida'
    Font.Style = []
    ParentFont = False
    OnClick = NButtClick
  end
end
