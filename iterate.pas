
function LegalValue( Val : Integer ) : Boolean;
begin
   if  ( Val < INFINITY  - 1 )  // This function returns   /
   and ( Val > -INFINITY + 1 )  // "FALSE"  if  a  value   /
       then Result := TRUE      // comes from interrupted  /
       else Result := FALSE;    // search.                 /
end;  // LegalValue

function TSearch.Iterate( Depth : Integer ) : Integer;
          var A, Val, HiVal, LoVal : Integer;
              FirstVal  : Integer;
              HasChoice : Boolean;
begin
   if Timing <> SEC_PER_MOVE   // TODO: saved seconds      /
      then AddSec := MoveTime  // management, perhaps even /
      else AddSec := 0;        // in SEC_PER_MOVE mode     /

   Result    := 0;
   FirstVal  := 0; // avoids compiler warning
   IsFailLow := FALSE;

   if CountLegalMoves > 1
      then HasChoice := TRUE
      else HasChoice := FALSE;

   {$IFNDEF XBOARD} Form1.mmAnalyze.Text := ''; {$ENDIF}
   MyStat.Clear;
   HiVal  :=  INFINITY + 1;
   LoVal  := -INFINITY - 1;
   NoMem  := FALSE;
   STi    := Time;

   for A := 1 to Depth do
   begin  //loop

     ETi := Time; // check the time limit - perhaps we can save
     if  ( SecondsBetween( STi, ETi ) - 1 > ( (MoveTime * 8) div 10 ) )
     and ( A > 5 )
     then begin                      // Program won't  start
       if ( Timing = MIN_PER_GAME )  // the  next  iteration
       or ( Timing = Periods )       // if it used more than
          then Exit;                 // 80% of time.
     end;

     DepthFinished := A;
     MaxExt := DepthFinished div 2; // Limiting depth of extensions

     Val := RootSearch(0, DepthFinished, LoVal, HiVal );

     if LegalValue( Val )
        then Result := Val;

     if ( Val >= HiVal )           // re-search
     or ( Val <= LoVal )
        then begin
          if  ( Val <= LoVal )     // Set  the appropriate
          and ( not TimeOut )      // flag if failing  low
          then begin               // to assign more time.
            IsFailLow := TRUE;
            {$IFNDEF XBOARD} Form1.AddToAnMemo('Fail low'); {$ENDIF}
          end;

          Val := RootSearch(0, DepthFinished, -INFINITY-1, INFINITY+1 );
          IsFailLow := FALSE;
          if LegalValue( Val )
             then Result := Val;
        end; // re-search

     HiVal := Val + ASP;    // Setting aspiration window.
     LoVal := Val - ASP;

     // Depth 1 search result is treated as an estimation
     // of  material value of position, used in  deciding
     // whether it is time to resign.

     if ( A = 1 )
        then FirstVal := Val;

     ETi := Time;
     SecUsed  := SecondsBetween( ETi, STi );
     TimeUsed := SecUsed;

     if TimeOut
        then begin
        (*
          {$IFDEF XBOARD}
          Form1.WBThread.SendCommand(
            IntToStr(Fixed) + ' ' + IntToStr(LastVal) + ' '
            + IntToStr(SecUsed*100) + ' '
            + IntToStr(MyStat.Nodes) + ' '
            + LastLine, TRUE);
          {$ENDIF}
        *)
          // check if it's time to resign
          if  ( LastVal  < RESIGN_VALUE )
          and ( FirstVal < RESIGN_BOARD_VAL ) // opponent is much better on the board, so he doesn't need to look for tactics
          then begin
              Inc( Want_Resign );
              if Want_Resign >= RESIGN_DURATION then
              {$IFDEF XBOARD} Form1.WBThread.SendCommand( 'resign', TRUE );
              {$ELSE} ShowMessage( 'I resign' );
              {$ENDIF}
          end
          else Want_Resign := 0;
          Exit;
        end;

     if  ( not HasChoice )      // Only one move possible  /
     and ( A > 2 ) then Exit;   // - get its value & exit. /

   end; // loop
end; // Iterate