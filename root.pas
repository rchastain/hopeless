
function TSearch.RootSearch(Ply, Depth, Alfa, Beta : Integer ) : Integer;
         var A, Temp, Best : Integer;
         LocalMoveList : TMoveSet;
         Current       : TMove;
         LocalIndex    : Integer;
         HTemp, HNew   : Integer;
         NewDepth      : Integer;

procedure ChangeOrder;
          var A, HNo, HFrom, HTo : Integer;
begin
   HNo := GetHashAddr( HTemp );
   if MainHash[ HNo ].Key = HTemp
   then begin
     HFrom := MainHash[ HNo ].SqFrom;
     HTo := MainHash[ HNo ].SqTo;
   end
   else begin
     HFrom := OUTSIDE;
     HTo := OUTSIDE;
   end;

   for A := 1 to LocalIndex do
   begin
     if  ( LocalMoveList[ A ].SqFr = HFrom )
     and ( LocalMoveList[ A ].SqTo = HTo )
         then LocalMoveList[ A ].Order := CAPT_SORT + CAPT_SORT;

     if  ( LocalMoveList[ A ].SqFr = MyMove.SqFr )
     and ( LocalMoveList[ A ].SqTo = MyMove.SqTo )
         then LocalMoveList[ A ].Order := High( Integer );
   end;
end; // ChangeOrder

procedure PickMove;
          var A, Max, Where : Integer;
begin
  Max := Low( Integer );
  Where := 1;

  for A := 1 to LocalIndex do
  if  ( LocalMoveList[ A ].Order > Max )
  and ( LocalMoveList[ A ].Flag < NOFLAG )
  then begin
     Where := A;
     Max := LocalMoveList[ A ].Order;
  end;

  Current := LocalMoveList[ Where ];
  LocalMoveList[ Where ].Flag := NOFLAG;
end; // PickMove

begin  // RootSearch
  Inc( MyStat.Nodes );
  Temp := -INFINITY;
  Best := -INFINITY;
  HTemp := InitHashValue;
  RepStack[ StackIndex ] := HTemp;

  GenMoves;
  LocalMoveList := MoveSet;
  LocalIndex := MoveNo;
  ChangeOrder;

  for A := 1 to LocalIndex do
  begin // loop

    if TimeOut  // the time is up and 4th iteration is completed
       then begin
         NoMem  := TRUE;
         Result := -INFINITY;
         Exit;
       end;

    PickMove;

    ETi := Time;
    {$IFNDEF XBOARD}   // additional information display
    Form1.mmInfo.Text := 'Depth ' + IntToStr( DepthFinished )
                   + ', move '  + IntToStr( A )
                   + '/' + IntToStr( LocalIndex ) + ' '
                   + MoveToStr( Current )
                   + LINE_END
                   + MyStat.KNodes + ' kNodes in '
                   + SecToClock( SecondsBetween( ETi, STi ) ) + ' ';

    if SecondsBetween( ETi, STi ) > 0
       then Form1.mmInfo.Text := Form1.mmInfo.Text + LINE_END
          + MyStat.Nps( ETi, STi );
    Form1.mmInfo.Text := Form1.mmInfo.Text + LINE_END;
    {$ENDIF}

    MakeMove( Current );

    // Program  extends checks in root node - in hope  that
    // search  results  will  be  more  uniform  this  way.

    if IsAttackedBy( Board.Side xor 1, Board.List[ListSide].Sqr, TRUE )
       then NewDepth := -1
       else NewDepth := 0;

    HNew := InitHashValue;
    if IsRep( HNew )
       then Temp := 0
       else begin
         if ( not DO_PVS )
         or ( A = 1 )
         or ( -MinimalWindowSearch(Ply+1, Depth-1, -Best-1, -Best, NewDepth, FALSE ) > Best )
            then begin
              if  ( DO_PVS )
              and ( A > 1 )
                  then Form1.mmAnalyze.Text := IntToStr(Depth) + '. ' + MoveToStr(Current) + ' [candidate] ' + LINE_END + Form1.mmAnalyze.Text;
              Temp := -Search(Ply+1, Depth-1, -Beta, -Alfa, NewDepth, FALSE );

              if  ( A = 1 )        // special case - failing
              and ( Temp <= Alfa ) // low on the first move
              and ( not TimeOut )
              then begin
                isFailLow := TRUE;
                Form1.mmAnalyze.Text := '[failed low on the first move] '
                + MoveToStr( Current ) + ' ' + IntToStr( Temp ) + LINE_END
                + Form1.mmAnalyze.Text;
              end;

            end;
       end;

    UnMakeMove;

    if  ( Temp > Best )
    and ( not TimeOut )
    then begin                  // Set a new score and    /
      Best := Temp;             // display search data.   /
      MyMove := Current;
      ThinkLine[ Depth ] := MoveToStr( Current )
                          + ' ' + ThinkLine[ Depth - 1 ];
      ETi := Time;
      SecUsed := SecondsBetween( ETi, STi );
      if ( Best > -INFINITY ) and ( Best > Alfa )
            then DisplayPV( Depth, Best );
    end; // new score and new best move

    if Best >= Beta
       then begin
         History.Update( Depth, Current.SqFr, Current.SqTo );
         Best := Beta;
         Break;
       end;

    if Best > Alfa
       then Alfa := Best;
  end; // loop

       Result := Best;
end; // RootSearch