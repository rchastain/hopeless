# Hopeless

## Overview

*Hopeless* is a Winboard chess engine written in Pascal by Pawel Koziol.

The engine has a built-in graphical interface, named *Clericus*.

Clericus is a Delphi project. The project was reactivated in 2019 by Roland Chastain.

Original Clericus website: http://pkoziol.cal24.pl/clericus/index.htm

## Credits

Hopeless uses WBUnit by dr Maciej Szmit. WBUnit was inspired by the work of Norbert Dudek (Winboard unit), Grzegorz Olczak (MyChess) and Giuseppe Canella (Geko).

The new Clericus Chess GUI uses [Silk Icons](http://www.famfamfam.com/lab/icons/silk/).

![alt text](screenshots/20200127.png)
