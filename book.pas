
// This file is a part of Clericus Chess project
// by Pawel Koziol, nescitus@o2.pl.

// It contains procedures managing the opening book
// (writing, deleting and flagging moves, picking them,
// loading and saving entire opening books) as well as
// those dealing with the comments displayed by opening
// book editor.

// --------------------------------------------------------

// Function "IsFlag" checks if TBook.BookMoveString returned
// a valid move or one of the special flags. Their meaning
// is:  'NONE' - no move,  'FILL' - no move, but there  is
// a continuation after at least one reply.

function TBook.IsFlag( MoveString : string ) : Boolean;
begin
   if ( MoveString = 'NONE' )
   or ( MoveString = 'FILL' )
      then Result := TRUE
      else Result := FALSE;
end; // IsFlag

function StrToMove( Command : string ) : TMove;
         var A : Integer;
         FromSq, ToSq : Byte;
begin
  FromSq := OUTSIDE;
  ToSq   := OUTSIDE;
  Result.SqFr := OUTSIDE;

  for A := A1 to H8 do
  begin
     if Command[ 1 ] + Command[ 2 ] = SqrName[ A ]
        then FromSq := A;
     if Command[ 3 ] + Command[ 4 ] = SqrName[ A ]
        then ToSq := A;
  end;

  GenMoves;
  for A := 1 to MoveNo do
  begin
     if  ( MoveSet[ A ].SqFr = FromSq )
     and ( MoveSet[ A ].SqTo = ToSq )
         then begin Result := MoveSet[ A ]; Exit; end;
  end;
end; // StrToMove

procedure TBook.ReadBookFile;
          var A : Integer;
          label Done;
begin
  NofEntries := 0;
  AssignFile( BookFile, BookName );
  if FileExists( BookName )
     then begin
       Reset( BookFile );
       for A := 1 to BOOK_SIZE do
       begin
         if Eof( BookFile )then goto Done;
         Read( BookFile, BookArray[ BookNo, A ] );
         if BookArray[ BookNo, A ].Freq > 0
            then Inc( NofEntries );
       end;
       Done:
       Close( BookFile );
     end;
end; // ReadBookFile

procedure TBook.ReadCommentFile;
          var A : Integer;
          label Done;
begin
  NofComments := 0;
  AssignFile( CommentFile, CommentName );
  if FileExists( CommentName )
     then begin
       Reset( CommentFile );
       for A := 1 to COMM_SIZE do
       begin
         if Eof( CommentFile )then goto Done;
         Read( CommentFile, CommentArray[ A ] );
         Inc( NofComments );
       end;
       Done:
       Close( BookFile );
     end;
end; // ReadCommentFile

procedure TBook.SortBookMove( From : Integer );
          var A : Integer;
          Swap  : TFullBook;
begin
   for A := From downto 2 do
     begin // loop
     if BookArray[ BookNo, A ].Fen < BookArray[ BookNo, A - 1 ].Fen
     then begin
        Swap := BookArray[ BookNo, A ];
        BookArray[ BookNo, A ] := BookArray[ BookNo, A - 1 ];
        BookArray[ BookNo, A - 1 ] := Swap;
     end
     else begin
       if  ( BookArray[ BookNo, A ].Fen = BookArray[ BookNo, A - 1 ].Fen )
       and ( BookArray[ BookNo, A ].Move < BookArray[ BookNo, A - 1 ].Move )
       then begin
         Swap := BookArray[ BookNo, A ];
         BookArray[ BookNo, A ] := BookArray[ BookNo, A - 1 ];
         BookArray[ BookNo, A - 1 ] := Swap;
       end
       else Break;
     end
     end; // loop
end; // SortBookMove

procedure TBook.WriteBookMove(Fen, Move: string);
var
  A: Integer;
begin
  for A := 0 to NofEntries do
    if (BookArray[BookNo, A].Fen = shortstring(Fen))
    and (BookArray[BookNo, A].Move = shortstring(Move))
    then begin
      Inc(BookArray[BookNo, A].Freq);
      Exit;
    end;
  BookArray[BookNo, NofEntries + 1].Fen := shortstring(Fen);
  BookArray[BookNo, NofEntries + 1].Move := shortstring(Move);
  BookArray[BookNo, NofEntries + 1].Freq := 10;
  Inc(NofEntries);
{$IFNDEF XBOARD}
  Inc(BookEditor.DispEntries);
{$ENDIF}
  SortBookMove(NofEntries);
end; // WriteBookMove

procedure TBook.WriteComment(Fen, Comment: string);
var
  A: Integer;
begin
  for A := 0 to NofComments do
    if CommentArray[ A ].Fen = shortstring(Fen)
    then begin
      CommentArray[A].Comment := shortstring(Comment);
      Exit;
    end;
  CommentArray[NofComments + 1].Fen := shortstring(Fen);
  CommentArray[NofComments + 1].Comment := shortstring(Comment);
  Inc(NofComments);
end; // WriteComment

function TBook.GetMoveValue( A : Integer ) : Integer;
begin
  Result := Random( BookArray[ BookNo, A ].Freq ) + 1;

     case BookArray[ BookNo, A ].Learn of
       B_EXCL : Result := Result + 1000;
       B_FUN  : Result := Result * 2;
       B_DISL : Result := Result div 2;
       B_BAD  : Result := Result - 2000;
     end; // of case
end; // GetMoveValue

function TBook.BookMoveString: string;
var
  A, Val, Max: Integer;
  BoardFen: string;
begin
  BoardFen := Form1.BoardToFen;
  Max := 0;
  Result := 'NONE';

  for A := 1 to NofEntries do
  if  ( BookArray[ BookNo, A ].Fen = TS78(BoardFen))
  and ( BookArray[ BookNo, A ].Freq > 0 )
  and ( BookArray[ BookNo, A ].Move <> 'FILL' )
  then begin // loop
     Val := GetMoveValue( A );

     if Val > Max
        then begin
          Max := Val;
          Result := string(BookArray[ BookNo, A ].Move);
        end;
  end; // loop

  if not IsFlag( Result )
     then OpeningMove := StrToMove( Result );
end;  // BookMoveString

function TBook.CommentString : string;
         var A : Integer;
         BoardFen : string;
begin
  BoardFen := Form1.BoardToFen;
  Result := '';
  for A := 1 to NofComments do
  if CommentArray[A].Fen = TS78(BoardFen)
     then begin
       Result := string(CommentArray[A].Comment);
       Exit;
     end;
end; // CommentString

procedure TBook.MarkBookMove( Fen, Move : string; Mark : Integer );
          var A : Integer;
begin
  for A := 0 to NofEntries do
  if  ( BookArray[ BookNo, A ].Fen = TS78(Fen) )
  and ( BookArray[ BookNo, A ].Move = shortstring(Move) )
  then begin
    BookArray[ BookNo, A ].Learn := Mark;
    Exit;
  end;
end; // MarkBookMove

procedure TBook.DeleteBookMove( Fen, Move : string );
          var A : Integer;
begin
  for A := 0 to NofEntries do
  if  ( BookArray[ BookNo, A ].Fen = TS78(Fen) )
  and ( BookArray[ BookNo, A ].Move = shortstring(Move) )
  then begin
    BookArray[ BookNo, A ].Freq := -100;
    Exit;
  end;
end; // DeleteBookMove

procedure TBook.ClearBookArray( BookNo : Integer );
          var A : Integer;
begin
   for A := 1 to BOOK_SIZE do
   BookArray[ BookNo, A ].Fen := '';
end; // ClearBookArray

procedure TBook.ClearCommentArray;
          var A : Integer;
begin
   for A := 1 to COMM_SIZE do
   begin
   CommentArray[ A ].Fen := '';
   CommentArray[ A ].Comment := '';
   end;
end; // ClearCommentArray

procedure TBook.SaveBook;
          var A : Integer;
begin
  AssignFile( BookFile, BookName );
  Rewrite( BookFile );
  for A := 1 to BOOK_SIZE do
  if BookArray[ BookNo, A ].Freq > 0
     then Write( BookFile, BookArray[ BookNo, A ] );
end; // SaveBook

procedure TBook.SaveComments;
          var A : Integer;
begin
  AssignFile( CommentFile, CommentName );
  Rewrite(CommentFile);
  for A := 1 to COMM_SIZE do
  if CommentArray[A].Fen <>''
  then
  Write( CommentFile, CommentArray[A] );
end; // SaveComments

procedure TBook.GetRandomLeaf;         // Looking for
          var BookMoveString : string; // a random leaf
begin                                  // position that
   BookMoveCount := 0;                 // both sides
   Form1.Execute( 'stop' );            // can reach.
   Form1.Execute( 'new' );
   repeat
      BookNo := 1;
      BookMoveString := MyBook.BookMoveString;
      if not IsFlag( BookMoveString )
         then begin
           Form1.Execute( BookMoveString );
           Inc( BookMoveCount );
         end;
   until IsFlag( BookMoveString );
   Form1.FenToGraphBoard( Form1.BoardToFen );
   {$IFNDEF XBOARD} BookEditor.RefreshForm; {$ENDIF}
end; // GetRandomLeaf

// Function "GetMissingMove" tries to find a move
// after which opponent will have a book reply.

function TBook.GetMissingMove : string;
         var A, LocalIndex : Integer;
         LocalMoveList : TMoveSet;
begin
   Result :='';
   GenMoves;
   LocalMoveList := MoveSet;
   LocalIndex := MoveNo;

   for A := 1 to LocalIndex do
   begin
       MakeMove( LocalMoveList[ A ] );
       if not IsFlag( BookMoveString )
          then Result := Result + MoveToStr( LocalMoveList[A] ) + ' ';
       UnMakeMove;
   end;
end; // GetMissingMove
