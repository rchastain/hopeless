
procedure SetLevel( Command : string );
          var PeriodString, MinString, IncString : string;
          PosInString : Integer;
          A : Integer;
begin
  PosInString := 0; // to avoid compiler warning
  SearchDepth := 28;
  if Command[ 7 ] = '0'
  then begin // either minutes per game or increment
     IncString := '';
     MinString := Command[ 9 ];
     for A := 10 to 14 do
     begin
       if Command[ A ] = ' '
          then begin
             PosInString := A + 1;
             Break;
          end;
       MinString := MinString + Command[ A ];
     end;

     for A := PosInString to Length( Command ) do
     begin
       if IsNumber( Command[ A ] )
          then IncString := IncString + Command[ A ]
          else Break;
     end;

     Timing := MIN_PER_GAME;
     BaseTime := StrToInt( MinString ) * 60;
     MoveTime := BaseTime div TimeDiv;
     BaseInc := StrToInt( IncString );
  end // minutes per game or increment
  else begin // x moves in y minutes
     PeriodString := Command[ 7 ];
     for A := 8 to 10 do
     begin
       if Command[ A ] = ' '
          then begin
            PosInString := A + 1;
            Break;
          end;
       PeriodString := PeriodString + Command[ A ];
     end;

     for A := PosInString to Length( Command ) do
     begin
       if IsNumber( Command[ A ] )
          then MinString := MinString + Command[ A ]
          else Break;
     end;

     Timing    := Periods;
     BaseMoves := StrToInt( PeriodString );
     MemMoves  := BaseMoves;
     BaseTime  := StrToInt( MinString ) * 60;
     MemTime := BaseTime;
     MoveTime   := BaseTime div TimeDiv;
     BaseInc   := 0;
  end; // x moves in y minutes
end; // SetLevel

procedure UpdateTime;
begin
  case Timing of
    MIN_PER_GAME : begin
       BaseTime := BaseTime - TimeUsed;
       MoveTime := BaseTime div TimeDiv;
       if BaseInc > 0
          then begin
            MoveTime := MoveTime + BaseInc;
            BaseTime := BaseTime + BaseInc;
          end;
    end; // minutes per game or increment

    PERIODS : begin
       Dec( BaseMoves );
       BaseTime := BaseTime - SecondsBetween( STi, ETi );
       if BaseMoves = 0
          then begin
            BaseMoves := MemMoves;
            BaseTime  := MemTime;
          end;

       MoveTime := ( BaseTime div 2 ) div BaseMoves
                +  ( BaseTime div 2 ) div TimeDiv;
    end; // Periods
  end; // of case
end; // UpdateTime
