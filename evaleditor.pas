unit evaleditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Spin, language;

type
  TEvalForm = class(TForm)
    ProgAttBar: TTrackBar;
    ProgAttEdit: TEdit;
    OppoAttBar: TTrackBar;
    OppoAttEdit: TEdit;
    KingPressLabel: TLabel;
    ProgMobBar: TTrackBar;
    ProgMobEdit: TEdit;
    OppoMobBar: TTrackBar;
    OppoMobEdit: TEdit;
    MobilityLabel: TLabel;
    BishPairEdit: TSpinEdit;
    BishPairValLabel: TLabel;
    DoublePawnValLabel: TLabel;
    DoubledEdit: TSpinEdit;
    ProgShieldBar: TTrackBar;
    OppoShieldBar: TTrackBar;
    OppoShieldEdit: TEdit;
    PawnShieldLabel: TLabel;
    PawnValLabel: TLabel;
    KnightValLabel: TLabel;
    QueenValLabel: TLabel;
    RookValLabel: TLabel;
    PawnValEdit: TSpinEdit;
    KnightValEdit: TSpinEdit;
    QueenValEdit: TSpinEdit;
    RookValEdit: TSpinEdit;
    BishValLabel: TLabel;
    BishValEdit: TSpinEdit;
    ScaleMatCheckBox: TCheckBox;
    CombinationCheckBox: TCheckBox;
    RookOpenEdit: TSpinEdit;
    RookOpenLabel: TLabel;
    RookHalfLabel: TLabel;
    RookHalfEdit: TSpinEdit;
    WeakPawnEdit: TSpinEdit;
    WeakPawnValLabel: TLabel;
    RookPairEdit: TSpinEdit;
    RookPairLabel: TLabel;
    BishPinEdit: TSpinEdit;
    BishPinLabel: TLabel;
    KnightAttBar: TTrackBar;
    BishAttBar: TTrackBar;
    RookAttBar: TTrackBar;
    QueenAttBar: TTrackBar;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    AttackLabel: TLabel;
    OutpostCheckBox: TCheckBox;
    CentPawnValEdit: TSpinEdit;
    RookPawnValEdit: TSpinEdit;
    CentPawnValLabel: TLabel;
    RookPawnValLabel: TLabel;
    KnightMobEdit: TSpinEdit;
    KnightMobLabel: TLabel;
    GrainEdit: TSpinEdit;
    GrainLabel: TLabel;
    RookKingEdit: TSpinEdit;
    RookPressKingLabel: TLabel;
    Natt: TEdit;
    BAtt: TEdit;
    RAtt: TEdit;
    Qatt: TEdit;
    WoodEdit: TSpinEdit;
    ImbalanceLabel: TLabel;
    KQDistEdit: TSpinEdit;
    QKDistLabel: TLabel;
    QEarlyEdit: TSpinEdit;
    QEarlyLabel: TLabel;
    NUnstableEd: TSpinEdit;
    BUnstableEd: TSpinEdit;
    NUnstableLabel: TLabel;
    BUnstableLabel: TLabel;
    CentColWeakEd: TSpinEdit;
    BMissLabel: TLabel;
    BlockedRookEd: TSpinEdit;
    RBlockedLabel: TLabel;
    NBlockPEd: TSpinEdit;
    NBlockPLabel: TLabel;
    BlockedDEEd: TSpinEdit;
    BlockedDELab: TLabel;
    KColorWeakEd: TSpinEdit;
    KColWeakLab: TLabel;
    NoFianchEd: TSpinEdit;
    SpoilFianchLabel: TLabel;
    ProgShieldEdit: TEdit;
    procedure ProgAttBarChange(Sender: TObject);
    procedure OppoAttBarChange(Sender: TObject);
    procedure ProgMobBarChange(Sender: TObject);
    procedure OppoMobBarChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BishPairEditChange(Sender: TObject);
    procedure DoubledEditChange(Sender: TObject);
    procedure ProgShieldBarChange(Sender: TObject);
    procedure OppoShieldBarChange(Sender: TObject);
    procedure CombinationCheckBoxClick(Sender: TObject);
    procedure ScaleMatCheckBoxClick(Sender: TObject);
    procedure OutpostCheckBoxClick(Sender: TObject);
    procedure PawnValEditChange(Sender: TObject);
    procedure CentPawnValEditChange(Sender: TObject);
    procedure RookPawnValEditChange(Sender: TObject);
    procedure KnightValEditChange(Sender: TObject);
    procedure RookValEditChange(Sender: TObject);
    procedure BishValEditChange(Sender: TObject);
    procedure QueenValEditChange(Sender: TObject);
    procedure RookOpenEditChange(Sender: TObject);
    procedure RookHalfEditChange(Sender: TObject);
    procedure RookPairEditChange(Sender: TObject);
    procedure BishPinEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GrainEditChange(Sender: TObject);
    procedure KnightMobEditChange(Sender: TObject);
    procedure RookKingEditChange(Sender: TObject);
    procedure KnightAttBarChange(Sender: TObject);
    procedure BishAttBarChange(Sender: TObject);
    procedure RookAttBarChange(Sender: TObject);
    procedure QueenAttBarChange(Sender: TObject);
    procedure WoodEditChange(Sender: TObject);
    procedure KQDistEditChange(Sender: TObject);
    procedure WeakPawnEditChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QEarlyEditChange(Sender: TObject);
    procedure NUnstableEdChange(Sender: TObject);
    procedure BUnstableEdChange(Sender: TObject);
    procedure CentColWeakEdChange(Sender: TObject);
    procedure BlockedRookEdChange(Sender: TObject);
    procedure NBlockPEdChange(Sender: TObject);
    procedure BlockedDEEdChange(Sender: TObject);
    procedure KColorWeakEdChange(Sender: TObject);
    procedure NoFianchEdChange(Sender: TObject);
  private
    { Private declarations }
  public
    procedure PolishLanguage;
    procedure EnglishLanguage;
    procedure LanguageSettings;
  end;

var
  EvalForm: TEvalForm;

implementation

uses main;

{$R *.dfm}

procedure TEvalForm.PolishLanguage;
begin
  PawnShieldLabel.Caption := 'Os�ona pionowa';
  KingPressLabel.Caption := 'Nacisk na kr�la';
  MobilityLabel.Caption := 'Ruchliwo��';
  PawnValLabel.Caption := 'Zwyk�y pion';
  CentPawnValLabel.Caption := 'Pion centralny';
  RookPawnValLabel.Caption := 'Pion skrajny';
  DoublePawnValLabel.Caption := 'Zdwojone piony';
  WeakPawnValLabel.Caption := 'S�aby pion';
  RookValLabel.Caption := 'Warto��';
  RookOpenLabel.Caption := 'Otwarta linia';
  RookHalfLabel.Caption := 'P�otwarta linia';
  RookPressKingLabel.Caption := 'Celuje w kr�la';
  RookPairLabel.Caption := 'Dwie wie�e';
  BishValLabel.Caption := 'Warto��';
  BishPairValLabel.Caption := 'Para go�c�w';
  BishPinLabel.Caption := 'Zwi�zanie';
  KnightValLabel.Caption := 'Warto��';
  KnightMobLabel.Caption := 'Dost�pne pole';
  QueenValLabel.Caption := 'Warto��';
  QKDistLabel.Caption := 'Dystans do kr�la';
  QEarlyLabel.Caption := 'Wczesne rozwini�cie';
  AttackLabel.Caption := 'atakowany przez';
  GrainLabel.Caption := 'Ziarnisto��';
  ImbalanceLabel.Caption := 'Modyfikator' + LINE_END + 'warto�ci' + LINE_END
    + 'figur';
  ScaleMatCheckBox.Caption := 'Skaluj materia�';
  OutpostCheckBox.Caption := 'Plac�wki';
  CombinationCheckBox.Caption := 'Kombinacje';
  BUnstableLabel.Caption := 'Niestabliny';
  NUnstableLabel.Caption := 'Niestabilny';
  BMissLabel.Caption := 'S�abo�� koloru (centrum)';
  RBlockedLabel.Caption := 'Blokowana przez kr�la';
  NBlockPLabel.Caption := 'Blokuje piona c';
  BlockedDELab.Caption := 'Zablokowany na D2 / E2';
  KColWeakLab.Caption := 'S�abo�� koloru (kr�l)';
  SpoilFianchLabel.Caption := 'Zepsute fianchetto';
end; // PolishLanguage

procedure TEvalForm.EnglishLanguage;
begin
  PawnShieldLabel.Caption := 'Pawn shield';
  KingPressLabel.Caption := 'King pressure';
  MobilityLabel.Caption := 'Mobility';
  PawnValLabel.Caption := 'Base value';
  CentPawnValLabel.Caption := 'Central pawn';
  RookPawnValLabel.Caption := 'Rook pawn';
  DoublePawnValLabel.Caption := 'Doubled';
  WeakPawnValLabel.Caption := 'Weak';
  RookValLabel.Caption := 'Base value';
  RookOpenLabel.Caption := 'Open file';
  RookHalfLabel.Caption := 'Half-open file';
  RookPressKingLabel.Caption := 'Oppo''s king file';
  RookPairLabel.Caption := 'Rook pair';
  BishValLabel.Caption := 'Base value';
  BishPairValLabel.Caption := 'Bishop pair';
  BishPinLabel.Caption := 'Pin bonus';
  KnightValLabel.Caption := 'Base value';
  KnightMobLabel.Caption := 'Mobility unit';
  QueenValLabel.Caption := 'Base value';
  QKDistLabel.Caption := 'King distance';
  QEarlyLabel.Caption := 'Early developement';
  AttackLabel.Caption := 'attacks';
  GrainLabel.Caption := 'Grain';
  ImbalanceLabel.Caption := 'Scale material';
  ScaleMatCheckBox.Caption := 'Skaluj materia�';
  OutpostCheckBox.Caption := 'Outposts';
  CombinationCheckBox.Caption := 'Combinations';
  NUnstableLabel.Caption := 'Unstable position';
  BUnstableLabel.Caption := 'Unstable position';
  BMissLabel.Caption := 'Center color weak';
  RBlockedLabel.Caption := 'Blocked by king';
  NBlockPLabel.Caption := 'Blocking c pawn';
  BlockedDELab.Caption := 'Blocked on D2 / E2';
  KColWeakLab.Caption := 'King color weak';
  SpoilFianchLabel.Caption := 'Spoiled fianchetto';
end; // EnglishLanguage

procedure TEvalForm.LanguageSettings;
begin
  case vLang of
    POLISH:
      PolishLanguage;
    ENGLISH:
      EnglishLanguage;
  end; // of case
end; // LanguageSettings

procedure TEvalForm.ProgAttBarChange(Sender: TObject);
begin
  ProgAttEdit.Text := IntToStr(ProgAttBar.Position);
  Form1.ProgAttack := ProgAttBar.Position;
end; // ProgAttBarChange

procedure TEvalForm.OppoAttBarChange(Sender: TObject);
begin
  OppoAttEdit.Text := IntToStr(OppoAttBar.Position);
  Form1.OppoAttack := OppoAttBar.Position;
end; // OppoAttBarChange

procedure TEvalForm.ProgMobBarChange(Sender: TObject);
begin
  ProgMobEdit.Text := IntToStr(ProgMobBar.Position);
  Form1.ProgMobility := ProgMobBar.Position;
end; // ProgMobBarChange

procedure TEvalForm.OppoMobBarChange(Sender: TObject);
begin
  OppoMobEdit.Text := IntToStr(OppoMobBar.Position);
  Form1.OppoMobility := OppoMobBar.Position;
end; // OppoMobBarChange

procedure TEvalForm.BishPairEditChange(Sender: TObject);
begin
  MyEval.B_PAIR := BishPairEdit.Value;
end; // BishPairEditChange

procedure TEvalForm.DoubledEditChange(Sender: TObject);
begin
  MyEval.P_DOUBLE := DoubledEdit.Value;
end; // DoubledEditChange

procedure TEvalForm.ProgShieldBarChange(Sender: TObject);
begin
  ProgShieldEdit.Text := IntToStr(ProgShieldBar.Position);
  Form1.ProgShield := ProgShieldBar.Position;
end; // ProgShieldBarChange

procedure TEvalForm.OppoShieldBarChange(Sender: TObject);
begin
  OppoShieldEdit.Text := IntToStr(OppoShieldBar.Position);
  Form1.OppoShield := OppoShieldBar.Position;
end; // OppoShieldBarChange

procedure TEvalForm.CombinationCheckBoxClick(Sender: TObject);
begin
  MyEval.DoPatterns := CombinationCheckBox.Checked;
end; // CombinationCheckBoxClick

procedure TEvalForm.ScaleMatCheckBoxClick(Sender: TObject);
begin
  MyEval.DoScaleMat := ScaleMatCheckBox.Checked;
end; // ScaleMatCheckBoxClick

procedure TEvalForm.OutpostCheckBoxClick(Sender: TObject);
begin
  MyEval.DoOutposts := OutpostCheckBox.Checked;
end; // OutpostCheckboxClick

procedure TEvalForm.PawnValEditChange(Sender: TObject);
begin
  MyEval.P_VALUE_N := PawnValEdit.Value;
end; // PawnValEditChange

procedure TEvalForm.CentPawnValEditChange(Sender: TObject);
begin
  MyEval.P_VALUE_DE := CentPawnValEdit.Value;
end; // CentPawnValEditChange

procedure TEvalForm.RookPawnValEditChange(Sender: TObject);
begin
  MyEval.P_VALUE_AH := RookPawnValEdit.Value;
end; // RookPawnValEditChange

procedure TEvalForm.KnightValEditChange(Sender: TObject);
begin
  MyEval.N_VALUE := KnightValEdit.Value;
end; // KnightValEditChange

procedure TEvalForm.RookValEditChange(Sender: TObject);
begin
  MyEval.R_VALUE := RookValEdit.Value;
end; // RookValEditChange

procedure TEvalForm.BishValEditChange(Sender: TObject);
begin
  MyEval.B_VALUE := BishValEdit.Value;
end; // BishValEditChange

procedure TEvalForm.QueenValEditChange(Sender: TObject);
begin
  MyEval.Q_VALUE := QueenValEdit.Value;
end; // QueenValEditChange

procedure TEvalForm.RookOpenEditChange(Sender: TObject);
begin
  MyEval.R_OPEN := RookOpenEdit.Value;
end; // RookOpenEditChange

procedure TEvalForm.RookHalfEditChange(Sender: TObject);
begin
  MyEval.R_HALF := RookHalfEdit.Value;
end; // RookHalfEditChange

procedure TEvalForm.RookPairEditChange(Sender: TObject);
begin
  MyEval.R_PAIR := RookPairEdit.Value;
end; // RookPairEditChange

procedure TEvalForm.BishPinEditChange(Sender: TObject);
begin
  MyEval.B_PIN := BishPinEdit.Value;
end; // BishPinEditChange

procedure TEvalForm.FormCreate(Sender: TObject);
begin
  Top := 1;
  Left := 1;
  LanguageSettings;
end; // FormCreate

procedure TEvalForm.GrainEditChange(Sender: TObject);
begin
  MyEval.GRAIN := GrainEdit.Value;
end; // GrainEditChange

procedure TEvalForm.KnightMobEditChange(Sender: TObject);
begin
  MyEval.N_MOB_UNIT := KnightMobEdit.Value;
end; // KnightMobEditChange

procedure TEvalForm.RookKingEditChange(Sender: TObject);
begin
  MyEval.R_PRESS_K := RookKingEdit.Value;
end; // RookKingEditChange

procedure TEvalForm.KnightAttBarChange(Sender: TObject);
begin
  MyEval.N_ATTACKS_K := KnightAttBar.Position;
  Natt.Text := IntToStr(KnightAttBar.Position);
end; // KnightAttBarChange

procedure TEvalForm.BishAttBarChange(Sender: TObject);
begin
  MyEval.B_ATTACKS_K := BishAttBar.Position;
  BAtt.Text := IntToStr(BishAttBar.Position);
end; // BishAttBarChange

procedure TEvalForm.RookAttBarChange(Sender: TObject);
begin
  MyEval.R_ATTACKS_K := RookAttBar.Position;
  RAtt.Text := IntToStr(RookAttBar.Position);
end; // RookAttBarChange

procedure TEvalForm.QueenAttBarChange(Sender: TObject);
begin
  MyEval.Q_ATTACKS_K := QueenAttBar.Position;
  Qatt.Text := IntToStr(QueenAttBar.Position);
end; // QueenAttBarChange

procedure TEvalForm.WoodEditChange(Sender: TObject);
begin
  MyEval.Imbalance := WoodEdit.Value;
end; // WoodEditChange

procedure TEvalForm.KQDistEditChange(Sender: TObject);
begin
  MyEval.Q_DIST_K := KQDistEdit.Value;
end; // KQDistEditChange

procedure TEvalForm.WeakPawnEditChange(Sender: TObject);
begin
  MyEval.P_WEAK := WeakPawnEdit.Value;
end; // WeakPawnEditChange

procedure TEvalForm.FormShow(Sender: TObject);
begin
  ProgShieldEdit.Text := IntToStr(Form1.ProgShield);
  ProgShieldBar.Position := Form1.ProgShield;
  OppoShieldEdit.Text := IntToStr(Form1.OppoShield);
  OppoShieldBar.Position := Form1.OppoShield;
  ProgAttEdit.Text := IntToStr(Form1.ProgAttack);
  ProgAttBar.Position := Form1.ProgAttack;
  OppoAttEdit.Text := IntToStr(Form1.OppoAttack);
  OppoAttBar.Position := Form1.OppoAttack;
  ProgMobEdit.Text := IntToStr(Form1.ProgMobility);
  ProgMobBar.Position := Form1.ProgMobility;
  OppoMobEdit.Text := IntToStr(Form1.OppoMobility);
  OppoMobBar.Position := Form1.OppoMobility;

  KnightAttBar.Position := MyEval.N_ATTACKS_K;
  Natt.Text := IntToStr(MyEval.N_ATTACKS_K);
  BishAttBar.Position := MyEval.B_ATTACKS_K;
  BAtt.Text := IntToStr(MyEval.B_ATTACKS_K);
  RookAttBar.Position := MyEval.R_ATTACKS_K;
  RAtt.Text := IntToStr(MyEval.R_ATTACKS_K);
  QueenAttBar.Position := MyEval.Q_ATTACKS_K;
  Qatt.Text := IntToStr(MyEval.Q_ATTACKS_K);

  PawnValEdit.Value := MyEval.P_VALUE_N;
  RookPawnValEdit.Value := MyEval.P_VALUE_AH;
  CentPawnValEdit.Value := MyEval.P_VALUE_DE;
  DoubledEdit.Value := MyEval.P_DOUBLE;
  WeakPawnEdit.Value := MyEval.P_WEAK;
  BlockedDEEd.Value := MyEval.P_BLOCKED_DE;

  KnightValEdit.Value := MyEval.N_VALUE;
  BishValEdit.Value := MyEval.B_VALUE;
  RookValEdit.Value := MyEval.R_VALUE;
  QueenValEdit.Value := MyEval.Q_VALUE;

  BishPairEdit.Value := MyEval.B_PAIR;
  BishPinEdit.Value := MyEval.B_PIN;
  BUnstableEd.Value := MyEval.B_UNSAFE;
  CentColWeakEd.Value := MyEval.B_CENT_MISS;
  KColorWeakEd.Value := MyEval.K_COLOR_WEAK;
  NoFianchEd.Value := MyEval.B_NO_FIANCH;

  RookPairEdit.Value := MyEval.R_PAIR;
  RookOpenEdit.Value := MyEval.R_OPEN;
  RookHalfEdit.Value := MyEval.R_HALF;
  RookKingEdit.Value := MyEval.R_PRESS_K;
  BlockedRookEd.Value := MyEval.K_BLOCK_R;

  KQDistEdit.Value := MyEval.Q_DIST_K;
  QEarlyEdit.Value := MyEval.Q_EARLY;

  KnightMobEdit.Value := MyEval.N_MOB_UNIT;
  NUnstableEd.Value := MyEval.N_UNSAFE;
  NBlockPEd.Value := MyEval.N_BLOCK_P;

  WoodEdit.Value := MyEval.Imbalance;
  GrainEdit.Value := MyEval.GRAIN;

  ScaleMatCheckBox.Checked := MyEval.DoScaleMat;
  OutpostCheckBox.Checked := MyEval.DoOutposts;
  CombinationCheckBox.Checked := MyEval.DoPatterns;
end; // FormShow

procedure TEvalForm.FormResize(Sender: TObject);
begin
end; // FormResize

procedure TEvalForm.QEarlyEditChange(Sender: TObject);
begin
  MyEval.Q_EARLY := QEarlyEdit.Value;
end;

procedure TEvalForm.NUnstableEdChange(Sender: TObject);
begin
  MyEval.N_UNSAFE := NUnstableEd.Value;
end;

procedure TEvalForm.BUnstableEdChange(Sender: TObject);
begin
  MyEval.B_UNSAFE := BUnstableEd.Value;
end;

procedure TEvalForm.CentColWeakEdChange(Sender: TObject);
begin
  MyEval.B_CENT_MISS := CentColWeakEd.Value;
end;

procedure TEvalForm.BlockedRookEdChange(Sender: TObject);
begin
  MyEval.K_BLOCK_R := BlockedRookEd.Value;
end;

procedure TEvalForm.NBlockPEdChange(Sender: TObject);
begin
  MyEval.N_BLOCK_P := NBlockPEd.Value;
end;

procedure TEvalForm.BlockedDEEdChange(Sender: TObject);
begin
  MyEval.P_BLOCKED_DE := BlockedDEEd.Value;
end;

procedure TEvalForm.KColorWeakEdChange(Sender: TObject);
begin
  MyEval.K_COLOR_WEAK := KColorWeakEd.Value;
end;

procedure TEvalForm.NoFianchEdChange(Sender: TObject);
begin
  MyEval.B_NO_FIANCH := NoFianchEd.Value;
end;

end.
