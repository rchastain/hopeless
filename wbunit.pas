
(* (C) dr Maciej Szmit 2005 *)

unit wbunit;

interface

uses
  Classes, Windows, Messages, SysUtils;

type
  TWbThread = class(TThread)
  private
    FHandle: THandle;
    FWinboardCommand: string;
  protected
    procedure Execute; override;
  public
    constructor Create(ACreateSuspended: Boolean; AHandle: THandle);
    procedure SendCommand(AMessage: string; ADoRecord: Boolean);
    procedure SendMove(AMove: string);
    property WinboardCommand: string read FWinboardCommand;
  end;

const
  CW_WBMSG = WM_USER + 1000;

var
  LogFile: TextFile;

implementation

constructor TWbThread.Create(ACreateSuspended: Boolean; AHandle: THandle);
begin
  inherited Create(ACreateSuspended);
  FHandle := AHandle;
  FWinboardCommand := '';
  AssignFile(LogFile, 'wbunit.log');
  Rewrite(LogFile);
end;

procedure TWbThread.Execute;
var
  s: string;
  c: char;
begin
  while TRUE do
  begin
    s := '';
    repeat
      try
        Read(input, c);
      except
        Suspend;
        Exit;
      end;
      if c <> #10 then
        s := s + c;
    until (c = #10);
    FWinboardCommand := s;
    WriteLn(LogFile, 'RECEIVING ' + s);
    SendCommand(s, FALSE);
    SendMessage(FHandle, CW_WBMSG, 0, 0);
  end;
end;

procedure TWbThread.SendCommand(AMessage: string; ADoRecord: Boolean);
begin
  if (Length(AMessage) <> 0) and (AMessage[1] <> 'O') then
    AMessage := Lowercase(AMessage);
  if ADoRecord then
    WriteLn(LogFile, 'SENDING ' + AMessage);
  Write(AMessage + #10);
  Flush(output);
end; // TWbThread.SendCommand

procedure TWbThread.SendMove(AMove: string);
begin
  SendCommand('move ' + AMove, TRUE);
  Flush(output);
end; // TWbThread.SendMove

end.
