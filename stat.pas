
procedure TStat.Clear;
begin
   Nodes := 0;       QNodes := 0;       PVNodes := 0;
   HHits := 0;       HCuts  := 0;       IIDeep  := 0;
   NullTries := 0;   NullCuts := 0;     Checks  := 0;
   FutCuts := 0;     RazorReds := 0;    LMReds  := 0;
   DeltaCuts := 0;
end; // ClrStats

function TStat.KNodes : string;
begin
  Result := IntToStr( Nodes div 1000 );
end; // KNodes

function TStat.Nps( STi, ETi : TDateTime ) : string;
begin
  if SecondsBetween( ETi, STi ) = 0
     then Result := ''
     else Result := IntToStr( Nodes div SecondsBetween( ETi, STi ) ) + ' Nps';
end; // Nps

function TStat.PercentStr( SmallNo, BigNo : Integer; Explanation : string ) : string;
begin
   if BigNo <> 0
      then Result := IntToStr( SmallNo * 100 div BigNo ) + '%' + Explanation
      else Result := '';
end; // PercentStr

function TStat.MakeString : string;
begin
   Result := IntToStr( Nodes ) + ' nodes ' + IntToStr( QNodes ) + ' quiesc nodes '
 + ', ' + PercentStr( QNodes, Nodes, ' of quiesc nodes' )
 + LINE_END + IntToStr( HHits ) + ' hash hits, '
 + IntToStr( HCuts ) + ' hash cutoffs'
 + ', '+ PercentStr( HCuts, HHits, ' of hash cutoffs' )
 + LINE_END + IntToStr( NullTries ) + ' null moves, '
 + IntToStr( NullCuts ) + ' null move cutoffs'
 + ', ' + PercentStr( NullCuts, NullTries, ' of null move cutoffs' )
 + LINE_END + IntToStr( Checks ) + ' check extensions'
 + LINE_END + IntToStr( IIDeep ) + ' IID ' + IntToStr( PVNodes ) + ' PV nodes' + LINE_END
 + 'futility cutoffs: ' + IntToStr(FutCuts) + ' Delta cutoffs: ' + IntToStr(DeltaCuts) + LINE_END
 + 'razoring: ' + IntToStr(RazorReds) + ', LMR: ' + IntToStr(LMReds);
end; // MakeString