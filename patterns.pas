
// This file is a part of "Clericus"  - a chess program
// by Pawel Koziol, nescitus@o2.pl.
//
// -----------------------------------------------------

// "Patterns.pas" contains only one big function, evaluating
// hand-crafted attack patterns. 

function TEval.Patterns : Integer;
begin
   Result := 0;

   // 1. Attack patterns resulting after Bxh7

   if  ( IsPiece( WHITE, KNIGHT, G5 ) )
   and ( IsPiece( WHITE, QUEEN, H5  ) )
   and ( IsPiece( BLACK, KING, G8 ) )
   and ( IsEmpty( H7 ) )
   and ( IsEmpty( H6 ) )
       then Result := Result + 200;

   if  ( IsPiece( WHITE, KNIGHT, B5 ) )
   and ( IsPiece( WHITE, QUEEN, A5  ) )
   and ( IsPiece( BLACK, KING, B8 ) )
   and ( IsEmpty( A7 ) )
   and ( IsEmpty( A6 ) )
       then Result := Result + 200;

   if  ( IsPiece( BLACK, KNIGHT, G4 ) )
   and ( IsPiece( BLACK, QUEEN, H4 ) )
   and ( IsPiece( WHITE, KING, G1 ) )
   and ( IsEmpty( H2 ) )
   and ( IsEmpty( H3 ) )
       then Result := Result - 200;

   if  ( IsPiece( BLACK, KNIGHT, B4 ) )
   and ( IsPiece( BLACK, QUEEN, A4 ) )
   and ( IsPiece( WHITE, KING, B1 ) )
   and ( IsEmpty( A2 ) )
   and ( IsEmpty( A3 ) )
       then Result := Result - 200;

   // 2. Attack patterns after a Bxh6 sacrifice
   //    - not sure if it's defined correctly

   if  ( IsPiece( WHITE, QUEEN, H6 ) )
   and ( IsPiece( BLACK, KING, G8 ) )
   and ( FileStat[ COL_H, BLACK ] = 0 )
   and ( FileStat[ COL_G, BLACK ] = 0 )
   and ( IsPiece( WHITE, BISH, D3 ) )
   and ( IsEmpty( E4 ) )
   and ( IsEmpty( F5 ) )
   and ( IsPiece( BLACK, PAWN, F7 ) )
   then Result := Result + 120;

   if  ( IsPiece( WHITE, QUEEN, A6 ) )
   and ( IsPiece( BLACK, KING, B8 ) )
   and ( FileStat[ COL_A, BLACK ] = 0 )
   and ( FileStat[ COL_B, BLACK ] = 0 )
   and ( IsPiece( WHITE, BISH, E3 ) )
   and ( IsEmpty( D4 ) )
   and ( IsEmpty( C5 ) )
   and ( IsPiece( BLACK, PAWN, C7 ) )
   then Result := Result + 120;

   if  ( IsPiece( BLACK, QUEEN, H3 ) )
   and ( IsPiece( WHITE, KING, G1 ) )
   and ( FileStat[ COL_H, WHITE ] = 0 )
   and ( FileStat[ COL_G, WHITE ] = 0 )
   and ( IsPiece( BLACK, BISH, D6 ) )
   and ( IsEmpty( E5 ) )
   and ( IsEmpty( F4 ) )
   and ( IsPiece( WHITE, PAWN, F2 ) )
   then Result := Result - 120;

   if  ( IsPiece( BLACK, QUEEN, A3 ) )
   and ( IsPiece( WHITE, KING, B1 ) )
   and ( FileStat[ COL_A, WHITE ] = 0 )
   and ( FileStat[ COL_B, WHITE ] = 0 )
   and ( IsPiece( BLACK, BISH, E6 ) )
   and ( IsEmpty( D5 ) )
   and ( IsEmpty( C4 ) )
   and ( IsPiece( WHITE, PAWN, C2 ) )
   then Result := Result - 120;

   // 3. Attack after a piece sacrifice on g5/g4
   // - helps only in defending against this trick ;(

   if  ( IsPiece( BLACK, PAWN, G4 ) )
   and ( FileStat[ COL_H, WHITE ] = 0 )
   and ( FileStat[ COL_H, BLACK ] = 0 )
   and ( IsPiece( BLACK, ROOK, H8 ) )
   and ( IsPiece( WHITE, KING, G1 ) )
   and ( Board.List[-2].Sqr <> OUTSIDE )
        then Result := Result - 75;

   if  ( IsPiece( BLACK, PAWN, B4 ) )
   and ( FileStat[ COL_A, WHITE ] = 0 )
   and ( FileStat[ COL_A, BLACK ] = 0 )
   and ( IsPiece( BLACK, ROOK, A8 ) )
   and ( IsPiece( WHITE, KING, B1 ) )
   and ( Board.List[-2].Sqr <> OUTSIDE )
        then Result := Result - 75;

   if  ( IsPiece( WHITE, PAWN, G5 ) )
   and ( FileStat[ COL_H, WHITE ] = 0 )
   and ( FileStat[ COL_H, BLACK ] = 0 )
   and ( IsPiece( WHITE, ROOK, H1 ) )
   and ( IsPiece( BLACK, KING, G8 ) )
   and ( Board.List[2].Sqr <> OUTSIDE )
        then Result := Result + 75;

   if  ( IsPiece( WHITE, PAWN, B5 ) )
   and ( FileStat[ COL_A, WHITE ] = 0 )
   and ( FileStat[ COL_A, BLACK ] = 0 )
   and ( IsPiece( WHITE, ROOK, A1 ) )
   and ( IsPiece( BLACK, KING, B8 ) )
   and ( Board.List[2].Sqr <> OUTSIDE )
        then Result := Result + 75;

   // 4. A defensive pattern: King hides
   // behind an opponent's pawn

   if  ( IsPiece( BLACK, PAWN, H2 ) )
   and ( IsPiece( WHITE, KING, H1 ) )
   then Result := Result + 120;

   if  ( IsPiece( BLACK, PAWN, A2 ) )
   and ( IsPiece( WHITE, KING, A1 ) )
   then Result := Result + 120;

   if  ( IsPiece( WHITE, PAWN, H7 ) )
   and ( IsPiece( BLACK, KING, H8 ) )
   then Result := Result + 120;

   if  ( IsPiece( WHITE, PAWN, A7 ) )
   and ( IsPiece( BLACK, KING, A8 ) )
   then Result := Result + 120;

// 5. Mate threat against a compromised fianchetto position

   if  ( IsPiece( WHITE, QUEEN, H6 ) )
   and ( IsPiece( BLACK, KING, G8 ) )
   and ( IsPiece( WHITE, PAWN, F6 ) or IsPiece( WHITE, BISH, F6 ) )
   and ( not IsAttackedBy( BLACK, G7, FALSE ) )
   then begin
     if Board.Side = WHITE
        then Result := Result + 1200
        else Result := Result + 400;
   end;

   if  ( IsPiece( WHITE, QUEEN, A6 ) )
   and ( IsPiece( BLACK, KING, B8 ) )
   and ( IsPiece( WHITE, PAWN, C6 ) or IsPiece( WHITE, BISH, C6 ) )
   and ( not IsAttackedBy( BLACK, B7, FALSE ) )
   then begin
     if Board.Side = WHITE
        then Result := Result + 1200
        else Result := Result + 200;
   end;

   if  ( IsPiece( BLACK, QUEEN, H3 ) )
   and ( IsPiece( WHITE, KING, G1 ) )
   and ( IsPiece( BLACK, PAWN, F3 ) or IsPiece( BLACK, BISH, F3) )
   and ( not IsAttackedBy( WHITE, G2, FALSE ) )
   then begin
     if Board.Side = BLACK
        then Result := Result - 1200
        else Result := Result - 200;
   end;

   if  ( IsPiece( BLACK, QUEEN, A3 ) )
   and ( IsPiece( WHITE, KING, B1 ) )
   and ( IsPiece( BLACK, PAWN, C3 ) or IsPiece( BLACK, BISH, C3) )
   and ( not IsAttackedBy( WHITE, B2, FALSE ) )
   then begin
     if Board.Side = BLACK
        then Result := Result - 1200
        else Result := Result - 200;
   end;

end; // Patterns
