program clericus;

uses
  Forms,
  main in 'main.pas' {Form1},
  promunit in 'promunit.pas' {PromForm},
  bookunit in 'bookunit.pas' {BookEditor},
  evaleditor in 'evaleditor.pas' {EvalForm},
  debugwindow in 'debugwindow.pas' {DebugForm},
  searched in 'searched.pas' {SearchEditor};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TPromForm, PromForm);
  Application.CreateForm(TBookEditor, BookEditor);
  Application.CreateForm(TEvalForm, EvalForm);
  Application.CreateForm(TDebugForm, DebugForm);
  Application.CreateForm(TSearchEditor, SearchEditor);
  Application.Run;
end.

